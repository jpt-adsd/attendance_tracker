<?php
  include $_SERVER['DOCUMENT_ROOT'].'/attendance_tracker/php_actions/conn.php';
  include $_SERVER['DOCUMENT_ROOT'].'/attendance_tracker/php_actions/core.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="assets/image/favicon.ico">
  <title>Attendance Tracker | Change Password</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Sto. Rosario Sapang Palay College | </b> <img src="assets/image/favicon-32x32.png">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg" >You are only one step a way to secure your password. Create a new one.</p>

      <form id='changePassword'>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Old Password" name="old_password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="New Password" name="new_password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Confirm Password" name="confirm_password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Change password</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-3 mb-1">
        <a href="<?= $_SESSION['home'] ?>">Home</a> <span id="msg"></span>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script type="text/javascript">
  $("#changePassword").submit(function(e){
    e.preventDefault();
    console.log('Login Submitted');
    var old_password = $('input[name="old_password"');
    var new_password = $('input[name="new_password"');
    var confirm_password = $('input[name="confirm_password"');
    var checkPassword = true;
    $("#msg").removeClass();
    $("#msg").text('');
    old_password.removeClass('is-invalid');
    new_password.removeClass('is-invalid');
    confirm_password.removeClass('is-invalid');
    if (!old_password.val()) {
      old_password.addClass('is-invalid');
    }
    if (!new_password.val()) {
      new_password.addClass('is-invalid');
    }
    if (!confirm_password.val()) {
      confirm_password.addClass('is-invalid');
    }
    if (new_password.val() != confirm_password.val() && new_password.val().length > 0 && confirm_password.val().length > 0) {
      checkPassword = false;
      new_password.addClass('is-invalid');
      confirm_password.addClass('is-invalid');
      $("#msg").removeClass();
      $("#msg").text('New password and confirm password not match.');
      $("#msg").addClass('badge badge-warning');
    }
    if (old_password.val() && new_password.val() && confirm_password.val()) {
      if (checkPassword) {
        $.ajax({
          url: 'php_actions/changePassword.php',
          type: 'post',
          dataType: 'json',
          data: $(this).serialize(),
          success: function(data){
            if (data.status == 'true') {
              $("#msg").text(data.message);
              $("#msg").removeClass();
              $("#msg").addClass('badge badge-success');
            } else {
              $("#msg").text(data.message);
              $("#msg").removeClass();
              $("#msg").addClass('badge badge-danger');
            }            
          },
          error: function(){
            alert('eww');
          }
        });
      }
    } else {
      $("#msg").removeClass();
      $("#msg").text('All fields are required.');
      $("#msg").addClass('badge badge-warning');
    }
  });  
</script>
</body>
</html>
