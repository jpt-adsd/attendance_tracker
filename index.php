<?php
  session_start();
  session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="assets/image/favicon.ico">
  <title>Attendance Tracker | Log in</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <b>Sto. Rosario Sapang Palay College | </b> <img src="assets/image/favicon-32x32.png">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form id="login">
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Username" name="username" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <span id="msg"></span>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- AdminLTE App 
<script src="dist/js/adminlte.min.js"></script>-->
<script type="text/javascript">
  $("#login").submit(function(e){
    e.preventDefault();
    console.log('Login Submitted');
    var username = $("input[name='username']");
    var password = $("input[name='password']");
    password.removeClass('is-invalid');
    username.removeClass('is-invalid');
    if (!username.val()) {
      username.addClass('is-invalid');
    }
    if (!password.val()) {
      password.addClass('is-invalid');
    }
    if (password.val() && username.val()) {
      $.ajax({
        url: 'php_actions/login.php',
        type: 'post',
        dataType: 'json',
        data: $(this).serialize(),
        success: function(data){
          if (data.status == 'true') {
            window.location.href = data.url;
          } else {
            $("#msg").text(data.message);
            $("#msg").removeClass();
            $("#msg").addClass('badge badge-danger');
          }
        },
        error: function(){
          alert('eww');
        }
      });
    } else {
      $("#msg").text('Please input username/password.');
      $("#msg").removeClass();
      $("#msg").addClass('badge badge-warning');
    }
  });
</script>
</body>
</html>
