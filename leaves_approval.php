<?php include 'layout/header.php'; ?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">File Leaves Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">File Leaves Management</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card row">
              <div class="card-header">
                <h3 class="card-title">File Leaves information</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <table id="users" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  	<th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">From - To</th>
                    <th scope="col">Leave Type</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                  </thead>

                  <tbody id="tbody_leaves">
                    
                  </tbody>

                  <tfoot>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">From - To</th>
                    <th scope="col">Leave Type</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
	</section>



<div class="modal fade" id="modal-approve">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<div class="alert alert-success alert-dismissible">
          <h5><i class="icon fas fa-check"></i> Approval confirmation!</h5>
          The action is irreversible! Please confirm to approve leave.<br>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" name="submit" class="btn btn-success" onclick="approveLeave()" data-dismiss="modal">Confirm</button>
      </div>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-reject">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible">
          <h5><i class="icon fas fa-times"></i> Rejection confirmation!</h5>
          The action is irreversible! Please confirm to reject leave.<br>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" name="submit" class="btn btn-danger" onclick="rejectLeave()" data-dismiss="modal">Confirm</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php include 'layout/footer.php'; ?>


<script type="text/javascript">
  var selectedFileLeaveId = "";

  loadFileLeaves();

  function loadFileLeaves(){
    $.ajax({
        url: 'php_actions/get_leaves.php',
        type: 'post',
        data: {
        },
        success: function (data) {
            //console.log(data.trim());
            $('#tbody_leaves').html(data.trim());
            
        },
        error: function(e){
            console.log(e.responseText);
        }
    });        
  }

  function approveLeave(){
    $.ajax({
        url: 'php_actions/approve-reject-leave.php',
        type: 'post',
        data: {
          action:"approve",
          fileLeaveId:selectedFileLeaveId
        },
        success: function (data) {
            console.log(data.trim());
            loadFileLeaves();
            
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
  }

  function rejectLeave(){
    $.ajax({
        url: 'php_actions/approve-reject-leave.php',
        type: 'post',
        data: {
          action:"reject",
          fileLeaveId:selectedFileLeaveId
        },
        success: function (data) {
            console.log(data.trim());
            loadFileLeaves();
            
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
  }

</script>