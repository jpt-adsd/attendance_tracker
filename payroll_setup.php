<?php
  include 'layout/header.php';
  if ($_SESSION['role'] != 'payroll') {
    echo '<script>window.location.href="'.$_SESSION['home'].'"</script>';
  }
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payroll Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">Payroll Management (Compensation)</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card row">
              <div class="card-header">
                <h3 class="card-title">Payroll Compensation</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <table id="payrollSetupTbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Category</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
  </section>


<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 400px;">
        <form id="compensation">
          <input type="hidden" name="id">
          <input type="hidden" name="type">
          <div class="form-group mb-3">
            <label for="name">Name</label>
            <input type="text" class="form-control" placeholder="Name" name="name" autocomplete="off">
          </div>
          <div class="form-group mb-3">
            <label for="category">Category</label>
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-3">
                <label><input type="radio" class="form-radio" name="category" value="0" checked> Allowance</label>    
              </div>
              <div class="col-md-3">
                <label><input type="radio" class="form-radio" name="category" value="1"> Deduction</label>
              </div>
            </div>
          </div>
          <div class="form-group mb-3">
            <label for="category">Status</label>
            <div class="row">
              <div class="col-md-3"></div>
              <div class="col-md-3">
                <label><input type="radio" class="form-radio" name="status" value="1" checked> Active</label>    
              </div>
              <div class="col-md-3">
                <label><input type="radio" class="form-radio" name="status" value="0"> Inactive</label>
              </div>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
        <button type="reset" name="reset" id="reset" class="btn btn-warning">Reset</button>
        <button type="button" data-dismiss="modal" id="close_modal" class="btn btn-danger">Cancel</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible">
          <h5><i class="icon fas fa-ban"></i> Delete confirmation!</h5>
          The action is irreversible! Please confirm to delete compensation.<br>
          <span id="deleteCompensationName"></span>
        </div>
        <form id="deleteCompensation">
          <input type="hidden" name="dId">
          <input type="hidden" name="dStatus">
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-danger">Confirm</button>
        <button type="button" data-dismiss="modal" id="close_modal_delete" class="btn btn-primary">Cancel</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php include 'layout/footer.php'; ?>

<script src="custom/payroll_setup.js"></script>

<?php
if ($_SESSION['role'] != 'payroll') {
  echo '<script>payrollDetailsTbl.buttons().disable();</script>';
}
?>