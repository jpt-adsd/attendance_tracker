
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=$ref_path?>plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=$ref_path?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=$ref_path?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?=$ref_path?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=$ref_path?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?=$ref_path?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?=$ref_path?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?=$ref_path?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=$ref_path?>plugins/moment/moment.min.js"></script>
<script src="<?=$ref_path?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=$ref_path?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=$ref_path?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=$ref_path?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=$ref_path?>dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=$ref_path?>dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=$ref_path?>dist/js/pages/dashboard.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?=$ref_path?>plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?=$ref_path?>plugins/jszip/jszip.min.js"></script>
<script src="<?=$ref_path?>plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?=$ref_path?>plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?=$ref_path?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?=$ref_path?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

<!-- jquery-validation -->
<script src="<?=$ref_path?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?=$ref_path?>plugins/jquery-validation/additional-methods.min.js"></script>

<!-- SweetAlert2 -->
<script src="<?=$ref_path?>plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?=$ref_path?>plugins/toastr/toastr.min.js"></script>
<script>
  var currentPage = window.location.pathname;
  if (currentPage.includes('<?=$ref_path?>users.php')) {
    $("#usersManagement").addClass('active');
  }
  if (currentPage.includes('<?=$ref_path?>payroll.php')) {
    $("#payrollManagement").addClass('active');
  }

</script>
</body>
</html>
