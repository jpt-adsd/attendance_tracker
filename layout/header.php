<?php
  $ref_path = "";
?>
<?php 
  include $_SERVER['DOCUMENT_ROOT'].'/attendance_tracker/php_actions/conn.php';
  include $_SERVER['DOCUMENT_ROOT'].'/attendance_tracker/php_actions/core.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="assets/image/favicon.ico">
  <title>Attendance Tracker | <?= ucfirst($_SESSION['role']) ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
  <style type="text/css">
    input[type=number]::-webkit-inner-spin-button, 
    input[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="navbar-nav ml-auto">
        <a class="nav-link" href="index.php" role="button">
          <i class="fas fa-sign-out-alt"> Logout</i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary bg-navy elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="assets/image/logo.jpg" alt="Attendance Tracker Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Attendance Tracker</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="changepassword.php" class="d-block"><?= ucwords($_SESSION['fullname']) ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item">
            <a href="dashboard.php" class="nav-link" id="dashboard_li">
              <i class="nav-icon fas fa-dashboard-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <?php if ($_SESSION['role'] != 'payroll') : ?>
          <li class="nav-item">
            <a href="leaves_approval.php" class="nav-link" id="fileLeavesManagement">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                File Leaves Management
              </p>
            </a>
          </li>
          <?php endif ?>
          <li class="nav-item" id="payrollManagement">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-ruble-sign"></i>
              <p>
                Payroll Management
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <?php if ($_SESSION['role'] != 'hr') : ?>
              <li class="nav-item">
                <a href="payroll.php" class="nav-link" id="payroll">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Payroll</p>
                </a>
              </li>
              <?php endif ?>
              
              <?php if ($_SESSION['role'] == 'payroll') : ?>
              <li class="nav-item">
                <a href="payroll_setup.php" class="nav-link" id="payrollSetup">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Compensation</p>
                </a>
              </li>
              <?php endif ?>

              <li class="nav-item">
                <a href="payroll_leave_credits.php" class="nav-link" id="payrollLeaveCredits">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Leave Credits</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="users.php" class="nav-link" id="usersManagement">
              <i class="nav-icon fas fa-users-cog"></i>
              <p>
                Users Management
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
