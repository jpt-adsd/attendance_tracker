<?php include 'layout/header.php'; ?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payroll Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">Payroll Management (Leave Credits)</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card row">
              <div class="card-header">
                <h3 class="card-title">Payroll Compensation</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <table id="payrollLeaveCreditsTbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Sick Leave</th>
                    <th scope="col">Vacation Leave</th>
                    <th scope="col">Maternity Leave</th>
                    <th scope="col">Bereavement Leave</th>
                    <th scope="col">Emergency Leave</th>
                    <?php if ($_SESSION['role'] != 'admin') : ?>
                    <th scope="col">Action</th>
                    <?php endif ?>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Sick Leave</th>
                    <th scope="col">Vacation Leave</th>
                    <th scope="col">Maternity Leave</th>
                    <th scope="col">Bereavement Leave</th>
                    <th scope="col">Emergency Leave</th>
                    <?php if ($_SESSION['role'] != 'admin') : ?>
                    <th scope="col">Action</th>
                    <?php endif ?>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
  </section>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle">Edit Leave Credits</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="editLeaveCredits">
        <div class="modal-body" style="overflow-y: auto;max-height: 400px;">
          <input type="hidden" name="id">
          <div class="form-group mb-3 row">
            <div class="col-md-12">
              <h3 id="fullName"></h3>
            </div>
          </div>
          <div class="form-group mb-3">
            <label for="firstName">Sick Leave</label>
            <input type="number" class="form-control" placeholder="Sick Leave" name="sl" autocomplete="off" value="0">
          </div>
          <div class="form-group mb-3">
            <label for="lastName">Vacation Leave</label>
            <input type="number" class="form-control" placeholder="Vacation Leave" name="vl" autocomplete="off" value="0">
          </div>
          <div class="form-group mb-3">
            <label for="contactNumber">Maternity Leave</label>
            <input type="number" class="form-control" placeholder="Maternity Leave" name="ml" autocomplete="off" value="0">
          </div>
          <div class="form-group mb-3">
            <label for="contactNumber">Bereavement Leave</label>
            <input type="number" class="form-control" placeholder="Bereavement Leave" name="bl" autocomplete="off" value="0">
          </div>
          <div class="form-group mb-3">
            <label for="contactNumber">Emergency Leave</label>
            <input type="number" class="form-control" placeholder="Emergency Leave" name="el" autocomplete="off" value="0">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
          <button type="reset" name="reset" id="reset" class="btn btn-warning">Reset</button>
          <button type="button" data-dismiss="modal" id="close_modal" class="btn btn-danger">Cancel</button>
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php include 'layout/footer.php'; ?>
<script src="custom/payroll_leave_credits.js"></script>