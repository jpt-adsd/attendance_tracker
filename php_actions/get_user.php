<?php
	include 'conn.php';

	$qry = "
		SELECT *, tu.id as user_id 
		FROM tblUsers tu
		LEFT JOIN tblLeaveCredits tlc on tlc.userId = tu.id
		where tu.id = " . $_POST['id'];
	$result = $conn->query($qry);
	$output = [];
	$i = 0;

	while ($row = $result->fetch_array()) {
		$output['id'] = $row['user_id'];
		$output['firstName'] = ucwords($row['firstName']);
		$output['lastName'] = ucwords($row['lastName']);
		$output['contactNumber'] = $row['contactNumber'];
		$output['userName'] = $row['userName'];
		$output['employeeId'] = $row['employeeId'];
		$output['role'] = $row['roleId'];
		break;
	}

	echo json_encode($output);

?>