<?php
	include 'conn.php';

	$fromDate = $_GET['fromDate'];
	$today = date('Y-m-d');
	$toDate = date('Y-m-d',strtotime($fromDate . "+7 days"));

	$dates = [];
	$currentDate = $fromDate;
	while ($currentDate < $toDate) {
		$dates[] = $currentDate;
		$currentDate = date('Y-m-d',strtotime($currentDate . "+1 days"));
	}

	$qry = "
		SELECT *
		FROM tblusers
		WHERE status = 1
		ORDER BY firstName, lastName
	";
	$result = $conn->query($qry);
	$output = [];
	$data = [];

	while ($row = $result->fetch_array()) {
		$userId = $row['id'];
		$data['name'][] = ucwords($row['firstName'] . ' ' . $row['lastName']);
		$data['id'][] = $userId;
		for ($i = 0; $i < 7; $i++) {
			if ($today > $dates[$i]) {
				$data[$userId][] = 'ABSENT';
			} else{
				$data[$userId][] = 'future';
			}
		}

		$qryLogs = "
			SELECT DATE_FORMAT(dateTimeIn, '%Y-%m-%d') as dateLogs
			FROM tblLogs
			WHERE personnelUserId = '$userId' AND dateTimeIn BETWEEN '$fromDate' AND '$toDate'
			ORDER BY dateLogs
		";
		$resultLogs = $conn->query($qryLogs);
		while ($rowLogs = $resultLogs->fetch_array()) {
			for ($i = 0; $i < 7; $i++) {
				if ($rowLogs['dateLogs'] == $dates[$i]) {
					$data[$userId][$i] = 'PRESENT';
				}
			}
		}

		$qryLeaves = "
			SELECT
				DATE_FORMAT(dateFrom, '%Y-%m-%d') as leavesFrom,
				DATE_FORMAT(dateTo, '%Y-%m-%d') as leavesTo,
				leavesType
			FROM tblfileleaves tfl
			INNER JOIN tblleavestype tlt on tfl.leaveTypeId = tlt.id
			WHERE userId = '$userId' AND (dateFrom BETWEEN '$fromDate' AND '$toDate' OR dateTo BETWEEN '$fromDate' AND '$toDate')
		";
		$resultLeaves = $conn->query($qryLeaves);
		while ($rowLeaves = $resultLeaves->fetch_array()) {
			$leavesFrom = $rowLeaves['leavesFrom'];
			$leavesTo = $rowLeaves['leavesTo'];
			while ($leavesFrom <= $leavesTo){
				for ($i = 0; $i < 7; $i++) {
					if ($leavesFrom == $dates[$i]) {
						$data[$userId][$i] = $rowLeaves['leavesType'];
					}
				}
				$leavesFrom = date('Y-m-d',strtotime($leavesFrom . "+1 days"));
			}
		}
	}

	for ($i = 0; $i < count($data['name']); $i++) {
		$name = $data['name'][$i];
		$id = $data['id'][$i];
		$day1 = $data[$id][0];
		$day2 = $data[$id][1];
		$day3 = $data[$id][2];
		$day4 = $data[$id][3];
		$day5 = $data[$id][4];
		$day6 = $data[$id][5];
		$day7 = $data[$id][6];
		$output['data'][] = [$name, $day1, $day2, $day3, $day4, $day5, $day6, $day7];
	}


	echo json_encode($output);
?>