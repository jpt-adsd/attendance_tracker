<?php
	include 'conn.php';

	$where = ' where 1';
	if ($_SESSION['role'] == 'admin') {
		$where = ' where u.roleId = 3';
	} elseif ($_SESSION['role'] == 'hr') {
		$where = ' where u.roleId != 3';
	}
	$query_str = "
			SELECT
				fl.id as 'fileLeaveId',
				(uhr.firstName + ' ' +uhr.lastName) as 'adminUserName',
				lt.leavesType,
				fl.status AS 'fileLeaveStatus',
				fl.*,
				u.*
			FROM tblfileleaves fl
			LEFT JOIN tblusers u ON fl.userId = u.id
			LEFT JOIN tblleavestype lt ON fl.leaveTypeId = lt.id
			LEFT JOIN tblusers uhr ON fl.hrUserId = uhr.id
			" . $where;
	$result = $conn->query($query_str);
	$i = 0;

	$statuses = array(
					"0"=>"Pending",
					"1"=>"Approved",
					"2"=>"Rejected"
	);
	while ($row = $result->fetch_array()) {
		$i++;
?>
		<tr>
			<td scope="col"><?=$i?></td>
            <td scope="col"><?=$row['firstName'] . " " . $row['lastName']?></td>
            <td scope="col"><?=$row['dateFrom'] . " - " . $row['dateFrom']?></td>
            <td scope="col"><?=$row['leavesType']?></td>
            <td scope="col"><?=$statuses[$row['fileLeaveStatus']]?></td>
            <td scope="col">
            	<a href="#" data-toggle="modal" data-target="#modal-approve" onclick="selectedFileLeaveId=<?=$row['fileLeaveId']?>"><span class="fa fa-check text-success"></span></a>
            	<a href="#" data-toggle="modal" data-target="#modal-reject" onclick="selectedFileLeaveId=<?=$row['fileLeaveId']?>"><span class="fa fa-times text-danger"></span></a>
            </td>
		</tr>

<?php
	}

?>