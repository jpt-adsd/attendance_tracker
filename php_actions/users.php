<?php
	include 'conn.php';

	$qry = "SELECT *,date_format(dateCreated, '%b %e, %Y') as since
	FROM tblUsers
	WHERE status < 2
	order by lastName, firstName";
	$result = $conn->query($qry);
	$output = [];
	$i = 0;
	$modalTitle = 'Edit User';
	while ($row = $result->fetch_array()) {
		$id = $row['id'];
		$employeeId = $row['employeeId'];
		$name = ucwords($row['firstName']) . ' ' . ucwords($row['lastName']);
		$contactNumber = $row['contactNumber'];
		$since = $row['since'];
		$admin = ($_SESSION['role'] != 'admin') ? 'disabled' : '';
		$status = ($row['status'] == 1) 
			? "<button class='form-control btn btn-success btn-sm style='text-align:center;' 
				onclick='updateStatus(".$id.", ".$row['status'].")' ".$admin.">Active</button>" 
			: "<button class='form-control btn btn-danger btn-sm color-palette' style='text-align:center'
				onclick='updateStatus(".$id.", ".$row['status'].")' ".$admin.">Inactive</button>";
		
		$actions = '
			<a type="button" data-toggle="modal" data-target="#modal-default" onclick="callUserModal('.$id.',1)">
				<i class="nav-icon fas fa-eye" title="View User"></i>
			</a>
		';
		if ($_SESSION['role'] == 'admin') {
			$actions .= '
				|
				<a type="button" data-toggle="modal" data-target="#modal-default" onclick="callUserModal('.$id.')">
					<i class="nav-icon fas fa-edit" title="Edit User"></i>
				</a>
				| 
				<a type="button" data-toggle="modal" data-target="#modal-delete" onclick="callUserDeleteModal('.$id.')">
					<i class="fas fa-trash-alt" title="Remove User"></i>
				</a>
			';

		}
		$output['data'][] = [$employeeId, $name, $contactNumber, $since, $status, $actions];
	}

	echo json_encode($output);

?>