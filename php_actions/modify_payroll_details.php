<?php
	include 'conn.php';

	$output = ['status' => false, 'message' => 'Error on payroll details modification.'];
	$postsAdd = ['payrollId', 'startDate', 'userId' ,'endDate', 'rate', 'hoursWorked'];
	$postsModify = ['id', 'mRate', 'mHoursWorked', 'cutoffSelection'];
	$postsDelete = ['dId', 'delete'];
	function checkPosts($posts){
		for ($i = 0; $i < count($posts); $i++) {
			if (!isset($_POST[$posts[$i]])) {
				return false;
			}
		}
		return true;
	}
	
	if (checkPosts($postsAdd)) {
		$payrollId = $_POST['payrollId'];
		$startDate = $_POST['startDate'];
		$endDate = $_POST['endDate'];
		$id = $_POST['userId'];
		$rate = $_POST['rate'];
		$hoursWorked = $_POST['hoursWorked'];
		if ($payrollId == 0) {
			$insertPayroll = "
				INSERT INTO tblPayrolls
					(`payrollUserId`, `hrsWorked`, `rate`, `cutoffStartDate`, `cutoffEndDate`)
				VALUES
					('$id', '$hoursWorked', '$rate', '$startDate', '$endDate')
			";
			if ($conn->query($insertPayroll)) {
				$payrollId = $conn->insert_id;
				$withAdditional = false;
				$withAdditionalError = false;
				foreach ($_POST as $key => $amount) {
				    if (str_contains($key, 'amount_')) {
				    	$id = substr($key, 7);
						$insertAdditional = "
							INSERT INTO tblAdditionalsOnPayrolls
								(payrollId, payrollSetupId, amount)
							VALUES
								('$payrollId', '$id', '$amount')
						";
						if ($conn->query($insertAdditional)) {
							$withAdditional = true;
						} else {
							$withAdditional = true;
							$withAdditionalError = true;
							$output = ['status' => false, 'message' => 'Error on payroll details modification.'];
							$conn->query("DELETE FROM tblAdditionalsOnPayrolls WHERE payrollId = '$payrollId'");
							$conn->query("DELETE FROM tblPayrolls WHERE id = '$payrollId'");
							break;
						}
					}
				}
				
				if (!$withAdditionalError) {
					if (isset($_POST['saveCutoff'])) {
						if ($_POST['saveCutoff']) {
							$_SESSION['startDate']  = $startDate;
							$_SESSION['endDate']  = $endDate;
						} else {
							$_SESSION['startDate']  = '';
							$_SESSION['endDate']  = '';
						}
						
					}
					$output = ['status' => true, 'message' => 'Payroll was successfully saved.'];
				} else {
					$output = ['status' => false, 'message' => 'Payroll was not successfully saved.'];
				}
			}
		}
	} elseif (checkPosts($postsModify)) {
		$payrollId = $_POST['cutoffSelection'];
		$rate = $_POST['mRate'];
		$hoursWorked = $_POST['mHoursWorked'];
		$updatePayroll = "
			UPDATE tblPayrolls
			SET hrsWorked = '$hoursWorked', rate = '$rate', dateUpdated = now()
			WHERE id = '$payrollId'
		";
		if ($conn->query($updatePayroll)) {
			foreach ($_POST as $key => $amount) {
			    if (str_contains($key, 'amount_')) {
			    	$id = substr($key, 7);
					$updateAdditional = "
						UPDATE tblAdditionalsOnPayrolls
						SET	amount = '$amount'
						WHERE payrollId = '$payrollId' AND payrollSetupId = '$id'
					";

					if ($conn->query($updateAdditional)) {
						$output = ['status' => true, 'message' => 'Payroll was successfully updated.'];
					}
				}
			}
		}
	} elseif (checkPosts($postsDelete)) {

		$conn->query('
			UPDATE
				tblPayrolls t1, tblAdditionalsOnPayrolls t2 
			SET 
				t2.status = 0,
				t1.status = 0
			WHERE
				t1.id = t2.payrollId
				AND t1.payrollUserId = ' . $_POST['dId']);
		$conn->query('UPDATE tblPayrolls SET status = 0 WHERE payrollUserId = '. $_POST['dId']);
					
		$output = ['status' => true, 'message' => 'Successfully delete payroll details.'];
	}

	echo json_encode($output);

?>