<?php
	include 'conn.php';


	$postId = $_POST['id'];
	$output = [];

	$qry = "SELECT * FROM tblUsers WHERE id = '$postId'	order by lastName, firstName";
	if ($result = $conn->query($qry)){
		$row = $result->fetch_array();
		$output['name'] = ucwords($row['firstName']) . ' ' . ucwords($row['lastName']);
		$output['cutoff'] = false;
		$output['id'] = $row['id'];
		$output['rate'] = 0;
	}
	$qryPayroll = "SELECT * FROM tblPayrolls WHERE status = 1 and payrollUserId = '$postId' ORDER BY dateCreated desc";
	$resultPayroll = $conn->query($qryPayroll);
	if ($resultPayroll->num_rows > 0) {
		$i = 0;
		while ($rowPayroll = $resultPayroll->fetch_array()) {
			if ($i == 0) {
				$output['rate'] = $rowPayroll['rate'];
			}
			$output['cutoff'] = true;
			$output['cutoff_dates'][] = $rowPayroll['cutoffStartDate'] . ' | ' . $rowPayroll['cutoffEndDate'];
			$output['cutoff_id'][] = $rowPayroll['id'];
			$i++;
		}
	}
	echo json_encode($output);
?>