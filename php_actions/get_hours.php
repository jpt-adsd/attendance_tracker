<?php
	include 'conn.php';
	
	$id = $_POST['id'];
	$startDate = $_POST['startDate'];
	$endDate = $_POST['endDate'];
	$output = [];
	$hour = 0;
	$checkPayrolls = "
		SELECT * FROM tblPayrolls 
		WHERE 
			payrollUserId = '$id' 
			AND ( 
					('$startDate' BETWEEN cutoffStartDate AND cutoffEndDate)
					OR ('$endDate' BETWEEN cutoffStartDate AND cutoffEndDate)
					OR (cutoffStartDate <= '$startDate' AND cutoffEndDate >= '$endDate')
					OR (cutoffStartDate >= '$startDate' AND cutoffEndDate <= '$endDate')
				)
			AND status = 1
	";
	$resultCheckPayrolls = $conn->query($checkPayrolls);
	if ($resultCheckPayrolls->num_rows > 0) {
		$output = ['status' => 'false', 'message' => 'The employee has already payroll on date range.', 'hours' => $hour];
	} else {

		$qry = "
			SELECT TIMESTAMPDIFF(MINUTE, dateTimeIn, dateTimeOut) AS hour
			FROM tblLogs
			WHERE
				personnelUserId = '$id'
				AND dateTimeIn
					BETWEEN '$startDate' AND '$endDate' + INTERVAL 1 DAY			
		";

		$result = $conn->query($qry);

		while ($row = $result->fetch_array()) {
			$hour += $row['hour'];
		}
		$hour /= 60;
		$output = ['status' => 'true', 'hours' => $hour];
	}
	echo json_encode($output);
?>