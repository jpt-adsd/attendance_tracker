<?php

include 'conn.php';

$month = (isset($_POST['month']) ? $_POST['month'] : date('M'));
$year = (isset($_POST['year']) ? $_POST['year'] : date('Y'));
$months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
$monthInNumeric = 0;
$data = [];
for ($i = 0; $i < count($months); $i++) {
	if ($month == $months[$i]) {
		$monthInNumeric = $i+1;
		break;
	}
}

$startMonthYear = $year . '-' . $monthInNumeric . '-1';
$endMonthYear = date("Y-n-t", strtotime($startMonthYear));
$endDay = date("t", strtotime($endMonthYear));
$qry = "
	SELECT *,
		CASE
			WHEN
				date_format(dateTo, '%Y-%c-%d') > '$endMonthYear'
				AND date_format(dateFrom, '%Y-%c-%d') < '$startMonthYear'
					THEN '$endDay'
			WHEN
				date_format(dateTo, '%Y-%c-%d') >= '$endMonthYear'
				AND date_format(dateFrom, '%Y-%c-%d') >= '$startMonthYear'
					THEN DATEDIFF('$endMonthYear', DATE_SUB(dateFrom, INTERVAL 1 DAY))
			WHEN
				date_format(dateTo, '%Y-%c-%d') <= '$endMonthYear'
				AND date_format(dateFrom, '%Y-%c-%d') <= '$startMonthYear'
					THEN DATEDIFF(dateTo, DATE_SUB('$startMonthYear', INTERVAL 1 DAY))
			ELSE DATEDIFF(dateTo, DATE_SUB(dateFrom, INTERVAL 1 DAY))
		END AS days
	FROM tblfileleaves tfl INNER JOIN tblusers tu on tu.id = tfl.userId INNER JOIN tblleavestype tlt on tlt.id = tfl.leaveTypeId
	WHERE tu.status = 1 AND tfl.status = 1 AND
		(
			(dateFrom >= '$startMonthYear' AND dateFrom <= '$endMonthYear')
			OR (dateTo >= '$startMonthYear' AND dateTo <= '$endMonthYear')
			OR (dateFrom <= '$startMonthYear' AND dateTo >= '$endMonthYear')
		)
	ORDER BY firstName, lastName
";
$result = $conn->query($qry);
while ($row = $result->fetch_array()) {
	$data['name'][] = ucwords($row['firstName'] . ' ' . $row['lastName']);
	$data['days'][] = $row['days'];
	$data['leaveType'][] = $row['leavesType'];
}

echo json_encode($data);

?>