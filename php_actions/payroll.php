<?php
	include 'conn.php';

	$qry = "
		SELECT 
			*,
			date_format(MAX(tp.dateCreated), '%b %e, %Y') as payrollCreation,
			u.id as userId,
			tp.id as payrollId
		FROM tblUsers u
		LEFT JOIN tblPayrolls tp on tp.payrollUserId = u.id  
		WHERE u.status = 1
		GROUP BY u.id
		order by lastName, firstName
	";
	$result = $conn->query($qry);
	$output = [];
	$i = 0;
	while ($row = $result->fetch_array()) {
		$id = $row['userId'];
		$payrollId = $row['payrollId'];
		$name = ucwords($row['firstName']) . ' ' . ucwords($row['lastName']);
		$lastActivity = ($row['payrollCreation'] == null) ? 'N/A' : $row['payrollCreation'];
		$actions = '
			<a type="button" data-toggle="modal" data-target="#modal-modify-payroll" onclick="modalPayroll('.$id.',1)">
				<i class="nav-icon fas fa-eye" title="View Details"></i>
			</a>
		';
		if ($_SESSION['role'] == 'payroll') {
			$actions .= '
			|
			<a type="button" data-toggle="modal" data-target="#modal-modify-payroll" onclick="modalPayroll('.$id.')">
				<i class="nav-icon fas fa-edit" title="Edit Details"></i>
			</a>
			| 
			<a type="button" data-toggle="modal" data-target="#modal-delete" onclick="modalPayrollDelete('.$id.')">
				<i class="fas fa-trash-alt" title="Remove Details"></i>
			</a>';
		}
		$output['data'][] = [++$i, $name, $lastActivity, $actions];
	}

	echo json_encode($output);

?>