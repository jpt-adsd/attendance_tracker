<?php
	include 'conn.php';

	$qry = "
		SELECT *
		FROM tblusers
		WHERE status < 2
	";
	$result = $conn->query($qry);
	$output = [];
	while ($row = $result->fetch_array()) {
		$id = $row['employeeId'];
		$uid = $row['id'];
		$name = ucwords($row['firstName'] . ' ' . $row['lastName']);
		$sickLeave = 'N/A';
		$vacationLeave = 'N/A';
		$maternityLeave = 'N/A';
		$bereavementLeave = 'N/A';
		$emergencyLeave = 'N/A';
		$qryLeaves = "SELECT * FROM tblLeaveCredits tlc INNER JOIN tblLeavesType tlt on tlt.id = tlc.leaveId WHERE userid = '$uid'";
		$resultLeaves = $conn->query($qryLeaves);
		while ($rowLeaves = $resultLeaves->fetch_array()) {
			if (strtoupper($rowLeaves['leavesType']) == 'SICK LEAVE') {
				$sickLeave = $rowLeaves['credit'];
			} elseif (strtoupper($rowLeaves['leavesType']) == 'VACATION LEAVE') {
				$vacationLeave = $rowLeaves['credit'];
			} elseif (strtoupper($rowLeaves['leavesType']) == 'MATERNITY LEAVE') {
				$maternityLeave = $rowLeaves['credit'];
			} elseif (strtoupper($rowLeaves['leavesType']) == 'BEREAVEMENT LEAVE') {
				$bereavementLeave = $rowLeaves['credit'];
			} elseif (strtoupper($rowLeaves['leavesType']) == 'EMERGENCY LEAVE') {
				$emergencyLeave = $rowLeaves['credit'];
			}  
		}
		if ($sickLeave == 'N/A') {
			$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$uid', 1, 15)");//SICK
		}
		if ($vacationLeave == 'N/A') {
			$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$uid', 2, 15)");//VACATION
		}
		if ($maternityLeave == 'N/A') {
			$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$uid', 3, 0)");//MATERNITY
		}
		if ($bereavementLeave == 'N/A') {
			$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$uid', 4, 7)");//BEREAVEMENT
		}
		if ($emergencyLeave == 'N/A') {
			$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$uid', 5, 0)");//EMERGENCY
		}
		$actions = '
			<a type="button" data-toggle="modal" data-target="#modal-default" onclick="callModal('
			."'".$uid."'".','
			."'".$name."'".','
			."'".$sickLeave."'".','
			."'".$vacationLeave."'".','
			."'".$maternityLeave."'".','
			."'".$bereavementLeave."'".','
			."'".$emergencyLeave."'"
			.')">
				<i class="nav-icon fas fa-edit" title="Edit Leave Credits">Edit</i>
			</a>
		';
		$output['data'][] = [$id, $name, $sickLeave, $vacationLeave, $maternityLeave, $bereavementLeave, $emergencyLeave, $actions];
	}

	echo json_encode($output);
?>