<?php
	include 'conn.php';

	$output = [];
	$output = ['status' => false, 'message' => 'User modification error.', 'action' => 'error'];
	if (
		isset($_POST['id'])
		&& isset($_POST['firstName'])
		&& isset($_POST['lastName'])
		&& isset($_POST['contactNumber'])
		&& isset($_POST['userName'])
		&& isset($_POST['employeeId'])
		&& isset($_POST['role'])
	) {
		$firstName = htmlentities($_POST['firstName']);
		$lastName = htmlentities($_POST['lastName']);
		$contactNumber = htmlentities($_POST['contactNumber']);
		$userName = htmlentities($_POST['userName']);
		$employeeId = htmlentities($_POST['employeeId']);
		$role = $_POST['role'];

		if ($_POST['id'] > 0) {
			$qry = "
				UPDATE tblUsers SET
					firstName = '$firstName',
					lastName = '$lastName',
					contactNumber = '$contactNumber',
					userName = '$userName',
					employeeId = '$employeeId',
					roleId = '$role',
					dateUpdated = now()
				WHERE id = '".$_POST['id']."'
			";
			if ($conn->query($qry)) {
				$output = [
					'status' => true,
					'message' => 'Successfully edit user.',
					'action' => 'edit'
					];
			}
		} else {

			$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		    $password = '';
		    $alphaLength = strlen($alphabet) - 1;
		    for ($i = 0; $i < 6; $i++) {
		        $n = rand(0, $alphaLength);
		        $password .= $alphabet[$n];
		    }
		    $textPassword = $password;
		    $password = md5($password);

			$qry = "
				INSERT INTO tblUsers
				(`firstName`, `lastName`, `contactNumber`, `userName`, `employeeId`, `roleId`, `password`)
				VALUES
				('$firstName', '$lastName', '$contactNumber', '$userName', '$employeeId', '$role', '$password')
			";
			if ($conn->query($qry)) {
				$id = $conn->insert_id;
				$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$id', 1, 15)");//SICK
				$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$id', 2, 15)");//VACATION
				$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$id', 3, 0)");//MATERNITY
				$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$id', 4, 7)");//BEREAVEMENT
				$conn->query("INSERT INTO tblleavecredits (userId, leaveId, credit) VALUES ('$id', 5, 0)");//EMERGENCY
				$output = [
					'status' => true,
					'message' => 'Successfully add user.',
					'temp_password' => $textPassword,
					'action' => 'add'
					];
			}
		}
	} elseif (isset($_POST['id'])) {
		if (isset($_POST['delete'])) {
			if (
				$_POST['delete'] == true
				&& $_POST['id'] > 0
			) {
				$qry = "UPDATE tblUsers SET status = 2, dateUpdated = now() WHERE id = " . $_POST['id'];
				if ($conn->query($qry)) {
					$output = [
						'status' => true,
						'message' => 'Successfully delete user.',
						'action' => 'delete'
						];
				}
			}
		}
		if (isset($_POST['update']) && isset($_POST['status'])) {
			if (
				$_POST['update'] == true
				&& $_POST['id'] > 0
			) {
				$newStatus = ($_POST['status'] == 0) ? 1 : 0;
				$qry = "UPDATE tblUsers SET status = '$newStatus', dateUpdated = now() WHERE id = " . $_POST['id'];
				if ($conn->query($qry)) {
					$message = ($newStatus == 0) ? 'inactivate' : 'activate';
					$output = [
						'status' => true,
						'message' => 'Successfully '.$message.' user.',
						'action' => 'status'
						];
				}
			}
		}
	}



	echo json_encode($output);
	

?>