<?php
	include 'conn.php';

	$postId = $_POST['id'];
	$qry = "SELECT *, u.id as userId
	FROM tblUsers u
	INNER JOIN tblPositions p on u.positionId = p.id
	WHERE u.id = '$postId'
	order by lastName, firstName";
	$result = $conn->query($qry);
	$output = [];
	$i = 0;
	while ($row = $result->fetch_array()) {
		$output['id'] = $row['userId'];
		$output['name'] = ucwords($row['firstName']) . ' ' . ucwords($row['lastName']);
		$output['position'] = $row['position'];

		$qryAdditionals = "SELECT * FROM tblAdditionalsOnPayrolls WHERE userId = '$postId'";
		$resultAdditionals = $conn->query($qryAdditionals);
		while ($rowAdditionals = $resultAdditionals->fetch_array()) {
			$output['field'][] = $rowAdditionals['field'];
			$output['addName'][] = $rowAdditionals['name'];
			$output['amount'][] = $rowAdditionals['amount'];
		}
	}

	echo json_encode($output);

?>