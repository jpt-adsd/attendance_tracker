<?php
	include 'conn.php';

	$postId = $_POST['payrollId'];

	$qryPayroll = "
		SELECT * 
		FROM tblPayrolls
		WHERE id = '$postId' and status = 1
	";
	$resultPayroll = $conn->query($qryPayroll);
	$rowPayroll = $resultPayroll->fetch_array();
	$output['hoursWorked'] = $rowPayroll['hrsWorked'];
	$output['rate'] = $rowPayroll['rate'];
	$output['addOns'] = false;
	$grossPay = $rowPayroll['rate'] * $rowPayroll['hrsWorked'];
	$deduction = 0;

	$qryAdditionals = "
		SELECT *, tpc.id as setup_id
		FROM tblAdditionalsOnPayrolls taop
		LEFT JOIN tblPayrollCompensation tpc on taop.payrollSetupId = tpc.id
		WHERE payrollId = '$postId' and taop.status = 1";
	$resultAdditionals = $conn->query($qryAdditionals);
	while ($rowAdditionals = $resultAdditionals->fetch_array()) {
		$output['addOns'] = true;
		$output['setup_id'][] = $rowAdditionals['setup_id'];
		$output['name'][] = $rowAdditionals['name'];
		$output['amount'][] = $rowAdditionals['amount'];
		if ($rowAdditionals['isDeduction']) {
			$deduction += $rowAdditionals['amount'];
		} else {
			$grossPay += $rowAdditionals['amount'];
		}
	}
	$netPay = $grossPay - $deduction;
	$output['grossPay'] = $grossPay;
	$output['netPay'] = $netPay;

	echo json_encode($output);

?>