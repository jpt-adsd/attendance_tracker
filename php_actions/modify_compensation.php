<?php
	include 'conn.php';

	$output = ['status' => false, 'message' => 'Error on payroll details modification.'];
	$postsAdd = ['id', 'type', 'name', 'category', 'status'];
	$postsStatus = ['id', 'status'];
	$postsDelete = ['dId', 'status'];
	function checkPosts($posts){
		for ($i = 0; $i < count($posts); $i++) {
			if (!isset($_POST[$posts[$i]])) {
				return false;
			}
		}
		return true;
	}
	
	if (checkPosts($postsAdd)) {
		$id = $_POST['id'];
		$type = $_POST['type'];
		$name = $_POST['name'];
		$category = $_POST['category'];
		$status = $_POST['status'];

		if ($type == 'add') {
			$insertCompensation = "
				INSERT INTO tblpayrollcompensation
					(`name`, `isDeduction`, `status`)
				VALUES
					('$name', '$category', '$status')
			";
			if ($conn->query($insertCompensation)) {
				$output = ['status' => true, 'message' => 'Compensation was successfully saved.'];
			}
		} elseif ($type == 'edit') {
			$updateCompensation = "
				UPDATE tblpayrollcompensation
				SET
					name = '$name', isDeduction = '$category', status = '$status', dateUpdated = now()
				WHERE id = '$id'
			";
			if ($conn->query($updateCompensation)) {
				$output = ['status' => true, 'message' => 'Compensation was successfully updated.'];
			}

		}
	} elseif (checkPosts($postsStatus)) {
		$id = $_POST['id'];
		$status = $_POST['status'];
		$statusText = ($status == 0) ? 'deactivated' : 'activated';
		$statusText = ($status == 2) ? 'deleted' : $statusText;

		$updateCompensation = "UPDATE tblpayrollcompensation SET status = '$status' WHERE id = '$id'";
		if ($conn->query($updateCompensation)) {
			$output = ['status' => true, 'message' => 'Compensation was successfully '.$statusText.'.'];
		}
	}


	echo json_encode($output);

?>