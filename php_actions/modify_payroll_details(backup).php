<?php
	include 'conn.php';

	$output = ['status' => false, 'message' => 'Error on payroll details modification.'];
	if (isset($_POST['additionalCounter']) && isset($_POST['oldCounter']) && isset($_POST['id'])) {
		$additionalCounter = $_POST['additionalCounter'];
		$oldCounter = $_POST['oldCounter'];
		$id = $_POST['id'];
		$status = false;
		for ($i = 1; $i <= $oldCounter; $i++) {
			$name = htmlentities($_POST['name_add'.$i]);
			$amount = $_POST['rate_add'.$i];
			$field = 'add'.$i;
			$qry = "
				UPDATE tblAdditionalsOnPayrolls 
				SET
					name = '$name',
					amount = '$amount'
				WHERE
					userId = '$id' AND field = '$field'
			";
			if ($conn->query($qry)) {
				$status = true;
			}
		}
		for ($i = $oldCounter+1; $i <= $additionalCounter; $i++) {
			$name = htmlentities($_POST['name_add'.$i]);
			$amount = $_POST['rate_add'.$i];
			$field = 'add'.$i;			
			$qry = "
				INSERT INTO tblAdditionalsOnPayrolls
					(userId, field, name, amount)
				VALUES
					('$id', '$field', '$name', '$amount')
			";
			if ($conn->query($qry)) {
				$status = true;
			}
		}
		if ($status) {
			$output = ['status' => true, 'message' => 'Successfully update payroll details.'];
		}
	} elseif (isset($_POST['id'])) {
		if (isset($_POST['delete'])) {
			if (
				$_POST['delete'] == true
				&& $_POST['id'] > 0
			) {
				$qry = "DELETE FROM tblAdditionalsOnPayrolls WHERE field != 'add1' AND userId = " . $_POST['id'];
				if ($conn->query($qry)) {
					$output = ['status' => true, 'message' => 'Successfully delete payroll details.'];
				}
			}
		}
	}


	echo json_encode($output);

?>