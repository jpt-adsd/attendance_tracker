<?php
include 'conn.php';
$semiAnnual = (isset($_POST['semiAnnual'])) ? $_POST['semiAnnual'] : 1;
$year = (isset($_POST['year'])) ? $_POST['year'] : date('Y');
$months = [1 => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'], 2 => ['Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']];
$startMonth = ($semiAnnual == 1) ? $year . '-1-01' : $year . '-7-01';
$endMonth = ($semiAnnual == 1) ? $year . '-6-31' : $year . '-12-31';
$data = [];


for ($i = 0; $i < 6; $i++) {
	$data['counter']['leaveType'][$i] = [0,0,0,0,0,0];
	$data['months'][] = $months[$semiAnnual][$i];
}
$qry = "
	SELECT
		date_format(dateFrom, '%Y-%c') as dateFrom,
		date_format(dateTo, '%Y-%c') as dateTo,
		date_format(dateFrom, '%c') as dateFromM,
		date_format(dateTo, '%c') as dateToM,
		leaveTypeId
	FROM tblfileleaves 
	WHERE status = 1 
	AND
		(
			(dateFrom >= '$startMonth' AND dateFrom <= '$endMonth')
			OR (dateTo >= '$startMonth' AND dateTo <= '$endMonth')
			OR (dateFrom <= '$startMonth' AND dateTo >= '$endMonth')
		)
";
$result = $conn->query($qry);
$data['status'] = 'false';
if ($result->num_rows > 0) {
	$data['status'] = true;
	while ($row = $result->fetch_array()) {
		$start = $row['dateFromM']-1;
		$end = $row['dateToM']-1;

		if ($semiAnnual == 1) {

			if ($start == $end) {
				$data['counter']['leaveType'][$row['leaveTypeId']][$start] += 1;
			} else {
				while ($start <= $end && $start < 6) {
					$data['counter']['leaveType'][$row['leaveTypeId']][$start] += 1;
					$start++;
				}
			}
		} else {
			$end -= 6;
			if ($start < 6) {
				$start = 0;
			}
			if ($start == $end) {
				$data['counter']['leaveType'][$row['leaveTypeId']][$start] += 1;
			} else {
				while ($start <= $end) {
					$data['counter']['leaveType'][$row['leaveTypeId']][$start] += 1;
					$start++;
				}				
			}
		}


	}
}


$qry = "SELECT * FROM tblusers WHERE status = 1";
$result = $conn->query($qry);
$data['total_users'] = $result->num_rows;

echo json_encode($data);
?>