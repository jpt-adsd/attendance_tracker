<?php
$servername = "localhost";
$username = "root";
$password = "";
$db = "dbattendancetracker";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
session_start();
$_SESSION['dir'] = 'http://localhost/attendance_tracker/';

?>