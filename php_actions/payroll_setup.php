<?php
	include 'conn.php';

	$qry = "
		SELECT *
		FROM tblPayrollCompensation
		WHERE status < 2
		ORDER BY isDeduction, name
	";
	$result = $conn->query($qry);
	$output = [];
	$i = 1;
	while ($row = $result->fetch_array()) {
		$name = ucwords($row['name']);
		$id = $row['id'];
		$isDeduction = ($row['isDeduction']) ? 'Deduction' : 'Allowance';
		$status = ($row['status'] == 1) 
			? "<button class='form-control btn btn-success btn-sm style='text-align:center;' 
				onclick='updateStatus(".$id.", 0)'>Active</button>" 
			: "<button class='form-control btn btn-danger btn-sm color-palette' style='text-align:center'
				onclick='updateStatus(".$id.", 1)'>Inactive</button>";
		$actions = '
			<a type="button" data-toggle="modal" data-target="#modal-default" onclick="getCompensationData('.$id.')">
				<i class="nav-icon fas fa-edit" title="Edit"></i>
			</a>
			| 
			<a type="button" data-toggle="modal" data-target="#modal-delete" onclick="getCompensationData('.$id.',1)">
				<i class="fas fa-trash-alt" title="Remove"></i>
			</a>
		';
		$output['data'][] = [$i, $name, $isDeduction, $status, $actions];
		$i++;
	}

	echo json_encode($output);
?>