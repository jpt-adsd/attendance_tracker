<?php

include 'conn.php';

$output = [];
$output = ['status' => 'false', 'message' => 'You\'re not allowed here.'];
if (isset($_POST['username']) && isset($_POST['password'])) {
	$username = $_POST['username'];
	$password = md5($_POST['password']);

	$qry = "SELECT * FROM tblusers WHERE userName = '$username' AND password = '$password' AND status = 1 LIMIT 1";
	$result = $conn->query($qry);
	if ($result->num_rows > 0) {
		$row = $result->fetch_array();
		$role = ['', 'admin', 'payroll', 'hr', 'user'];
		if ($role[$row['roleId']] == 'user') {
			$output = ['status' => 'false', 'message' => 'You\'re not authorized here.'];
		} else {
			
			$url = ['', 'users.php', 'payroll.php', 'leaves_approval.php'];

			$_SESSION['home'] = $url[$row['roleId']];
			$_SESSION['id'] = $row['id'];
			$_SESSION['role'] = $role[$row['roleId']];
			$_SESSION['fullname'] = ucwords($row['firstName'] . ' ' . $row['lastName']);
			$output = ['status' => 'true', 'message' => 'Login successful.', 'url' => $url[$row['roleId']]];
		}
	} else {
		$output = ['status' => 'false', 'message' => 'Incorrect username/password.'];
	}
}

echo json_encode($output);

?>