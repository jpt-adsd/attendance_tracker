<?php
  include 'layout/header.php';
  if ($_SESSION['role'] == 'hr') {
    echo '<script>window.location.href="'.$_SESSION['home'].'"</script>';
  }
?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Payroll Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">Payroll Management (Payroll)</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card row">
              <div class="card-header">
                <h3 class="card-title">Payroll</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <table id="payrollTbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  	<th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Last Activity</th>
                    <th scope="col">Action</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Last Activity</th>
                    <th scope="col">Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
	</section>

<div class="modal fade" id="modal-modify-payroll">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitlePayroll"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 400px">
        <form id="payrollDetails">
          <input type="hidden" name="id">
          <div class="form-group mb-3 row">
            <div class="col-md-12">
              <h3 id="fullName"></h3>
            </div>
          </div>
          <div class="form-group mb-3 row" id='divPay'>
            <div class="col-md-6">
              <label for="rate">Gross Pay</label>
              <input type="number" name="grossPay" step=".01" placeholder="Amount"
                style="text-align: right;" class="form-control">
            </div>
            <div class="col-md-6">
              <label for="hoursWorked">Net Pay</label>
              <input type="number" name="netPay" placeholder="Amount" step=".01" 
                style="text-align: right;" class="form-control">
            </div>
          </div>
          <div id="additionals"></div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
        <button type="button" data-dismiss="modal" id="close_modal" class="btn btn-danger">Close</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-add-payroll">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 400px">
        <form id="payrollAdd">
          <input type="hidden" name="payrollId" value="0">
          <div class="form-group mb-3 row">
            <div class="col-md-6">
              <label for="startDate">Start Date</label>
              <input type="date" name="startDate" class="form-control"
                value="<?= (isset($_SESSION['startDate'])) ? $_SESSION['startDate'] : '' ?>">
            </div>
            <div class="col-md-6">
              <label for="endDate">End Date</label>
              <input type="date" name="endDate" class="form-control"
                value="<?= (isset($_SESSION['startDate'])) ? $_SESSION['endDate'] : '' ?>">
            </div>
          </div>
          <div class="form-group mb-3 row">
            <div class="col-md-6">
              <label for="userId">Employee List</label>
              <select name="userId" class="form-control">
                <option value="">~~~Select Employee~~~</option>
                <?php 
                    $qry = "SELECT * FROM tblUsers WHERE status = 1 ORDER BY lastName, firstName";
                    $result = $conn->query($qry);
                    while ($row = $result->fetch_array()) {
                    $id = $row['id'];
                    $name = ucwords($row['firstName']) . ' ' . ucwords($row['lastName']);
                ?>
                <option value="<?= $id ?>"><?= $name ?></option>
                <?php
                    }
                ?>
              </select>
            </div>
            <div class="col-md-3">
              <label for="rate">Rate (Daily)</label>
              <input type="number" name="rate" step=".01" placeholder="Amount"
                style="text-align: right;" class="form-control">
            </div>
            <div class="col-md-3">
              <label for="hoursWorked">Hours Worked</label>
              <input type="number" name="hoursWorked" placeholder="# of Hours" step=".01" class="form-control">
            </div>
          </div>
          <?php
              $qrySetUp = "SELECT * FROM tblpayrollcompensation where status = 1";
              $resultSetUp = $conn->query($qrySetUp);
              $setup = [];
              $countCompensation = 0;
              while ($rowSetUp = $resultSetUp->fetch_array()){
                $setup['id'][] = $rowSetUp['id'];
                $setup['name'][] = $rowSetUp['name'];
                $countCompensation++;
              }

              for ($i = 0; $i < $countCompensation; $i+=2) {
          ?>
                <div class="form-group mb-3 row">
                  <div class="col-md-3">
                    <label for="amount_<?= $setup['id'][$i] ?>"><?= $setup['name'][$i] ?></label>
                  </div>
                  <div class="col-md-3">
                    <input type="number" name="amount_<?= $setup['id'][$i] ?>" step=".01" placeholder="Amount"
                      style="text-align: right;" class="form-control">
                  </div>
          <?php if (isset($setup['id'][$i+1])) : ?>        
                  <div class="col-md-3">
                    <label for="amount_<?= $setup['id'][$i+1] ?>"><?= $setup['name'][$i+1] ?></label>
                  </div>
                  <div class="col-md-3">
                    <input type="number" name="amount_<?= $setup['id'][$i+1] ?>" step=".01" placeholder="Amount"
                      style="text-align: right;" class="form-control">
                  </div>
                </div>
          <?php endif; ?>

          <?php
              }
          ?>
          <input type="hidden" name="total_setup" value="<?= $countCompensation ?>">
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" id="submitAddPayroll" class="btn btn-primary">Submit</button>
        <button type="button" data-dismiss="modal" id="modal_add_close" class="btn btn-danger">Close</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div class="modal fade" id="modal-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <div class="alert alert-danger alert-dismissible">
          <h5><i class="icon fas fa-ban"></i> Delete confirmation!</h5>
          The action is irreversible! Please confirm to delete the payroll details from user.<br>
          <span id="deleteFullname"></span>
        </div>
        <form id="deletePayrollDetails">
          <input type="hidden" name="dId">
          <input type="hidden" name="delete">
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-danger">Confirm</button>
        <button type="button" data-dismiss="modal" id="close_modal_delete" class="btn btn-primary">Cancel</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php include 'layout/footer.php'; ?>
<script src="custom/payroll.js"></script>
<?php
  if ($_SESSION['role'] != 'payroll') {
    echo '<script>payrollDetailsTbl.buttons().nodes().css("display", "none");</script>';
  }
?>