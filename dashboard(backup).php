<?php include 'layout/header.php';?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            
            <div class="card row">
              <div class="card-header">
                <h3 class="card-title">Dashboard</h3>
              </div>
              <!-- /.card-header -->

              <div class="card-body col-md-12">
                <div class="row mb-2">
                  <div class="col-md-2">
                    <label for="fromDate">From Date:</label>
                  </div>
                  <div class="col-md-3">
                    <input type="date" name="fromDate" value="<?= date('Y-m-d') ?>" class="form-control">
                  </div>
                </div>
                <table id="dashboardTbl" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  	<th scope="col">Name</th>
                    <?php
                      $fromDate = (isset($_POST['fromDate']) ? $_POST['fromDate'] : date('d-M'));
                    ?>
                    <?php for ($i = 0; $i < 7; $i++) : ?>
                      <th scope="col"><?= date('d-M',strtotime($fromDate . "+".$i." days")) ?></th>
                    <?php endfor ?>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                  	<th>Name</th>
                    <?php for ($i = 0; $i < 7; $i++) : ?>
                      <th scope="col"><?= date('d-M',strtotime($fromDate . "+".$i." days")) ?></th>
                    <?php endfor ?>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
	</section>

<?php include 'layout/footer.php'; ?>
<script>
  var fromDate = $("input[name='fromDate']").val();
  var dashboardTbl = $('#dashboardTbl').DataTable({
    "ajax": "php_actions/dashboard.php?fromDate="+fromDate
  });

  $("input[name='fromDate']").change(function(){
    dashboardTbl.clear().destroy();
    var fromDate = $("input[name='fromDate']").val();
    dashboardTbl = $('#dashboardTbl').DataTable({
      "ajax": "php_actions/dashboard.php?fromDate="+fromDate
    });
    $.ajax({
      url: 'dashboard.php',
      type: 'POST',
      data: {fromDate:fromDate}
    });
  });
</script>
