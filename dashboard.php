<?php include 'layout/header.php';?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">

            <div class="card row">
                          <!-- BAR CHART -->
              <div class="card card-info">
                <div class="card-header">
                  <h3 class="card-title">Leave Chart</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-2">
                      <select name="semiAnnual" class="form-control">
                        <option value="1">Jan to Jun</option>
                        <option value="2">Jul to Dec</option>
                      </select>  
                    </div>
                    <div class="col-md-2">
                      <input type="number" name="year" class="form-control" value="<?= date('Y') ?>">
                    </div>
                    <div class="col-md-2">
                      <input type="button" onclick="show()" class="btn btn-primary" value="Show">
                    </div>
                  </div>
                  <div class="chart">
                    <canvas id="barChart" style="min-height: 250px; height: 300px; max-height: 300px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.row -->
          </div>
        </div>
      </div>
	</section>

<?php include 'layout/footer.php'; ?>
<script type="text/javascript">
function show() {
var semiAnnual = $("select[name='semiAnnual']").val();
var year = $("input[name='year']").val();
  $.ajax({
    url: 'php_actions/dashboard.php',
    type: 'post',
    dataType: 'json',
    data: {semiAnnual:semiAnnual, year:year},
    success: function (data){

      if (data.status == 'false') {
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        Toast.fire({
          icon: 'warning',
          title: ' No record found.'
        });
      }

      var areaChartData = {
        labels  : data.months,
        datasets: [
          {
            label               : 'Emergency Leave',
            backgroundColor     : 'rgb(233,114,77)',
            borderColor         : 'rgb(233,114,77)',
            pointRadius          : false,
            pointColor          : '#3b8bba',
            pointStrokeColor    : 'rgb(233,114,77)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(233,114,77)',
            data                : data.counter.leaveType[5]
          },
          {
            label               : 'Bereavement Leave',
            backgroundColor     : 'rgb(134,134,134)',
            borderColor         : 'rgb(134,134,134)',
            pointRadius         : false,
            pointColor          : 'rgb(134,134,134)',
            pointStrokeColor    : 'rgb(134,134,134)',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(134,134,134)',
            data                : data.counter.leaveType[4]
          },
          {
            label               : 'Maternity/Paternity Leave',
            backgroundColor     : 'rgb(214,215,39)',
            borderColor         : 'rgb(214,215,39)',
            pointRadius         : false,
            pointColor          : 'rgb(214,215,39)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(214,215,39)',
            data                : data.counter.leaveType[3]
          },
          {
            label               : 'Vacation Leave',
            backgroundColor     : 'rgb(146,202,209)',
            borderColor         : 'rgb(146,202,209)',
            pointRadius         : false,
            pointColor          : 'rgb(146,202,209)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(146,202,209)',
            data                : data.counter.leaveType[2]
          },
          {
            label               : 'Sick Leave',
            backgroundColor     : 'rgb(121,204,179)',
            borderColor         : 'rgb(121,204,179)',
            pointRadius         : false,
            pointColor          : 'rgb(121,204,179)',
            pointStrokeColor    : '#c1c7d1',
            pointHighlightFill  : '#fff',
            pointHighlightStroke: 'rgb(121,204,179)',
            data                : data.counter.leaveType[1]
          }
        ]
      };

      var barChartCanvas = $('#barChart').get(0).getContext('2d')
      var barChartData = $.extend(true, {}, areaChartData)
      var temp0 = areaChartData.datasets[0]
      var temp1 = areaChartData.datasets[1]
      barChartData.datasets[0] = temp1
      barChartData.datasets[1] = temp0

      var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : false,
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Month'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'No. of Employees'
            },
            ticks: {
              min: 0,
              max: data.total_users,
              stepSize: 10
            }
          }]
        }
      };

      var myChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
      });

      var canvas = document.getElementById("barChart");
      canvas.onclick = function(evt) {
        var activePoints = myChart.getElementsAtEvent(evt);
        if (activePoints[0]) {
          var chartData = activePoints[0]['_chart'].config.data;
          var idx = activePoints[0]['_index'];

          var month = chartData.labels[idx];
          var year = $("input[name='year']").val();
          $.ajax({
            url: 'php_actions/dashboard_details.php',
            type: 'post',
            dataType: 'json',
            data: {month:month, year:year},
            success: function (data) {
              var output = '';
              for (var x = 0; x < data.name.length; x++) {
                output += 'Name: ' + data.name[x] + '\n';
                output += 'Days: ' + data.days[x] + '\n';
                output += 'Leave: ' + data.leaveType[x] + '\n\n';
              }
              alert(output);
            }
          });
        }
      };
    } //end success function
  }); //end ajax request
}// end function
show();
</script>