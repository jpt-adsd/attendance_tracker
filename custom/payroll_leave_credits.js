var payrollLeaveCreditsDetailsTbl = $('#payrollLeaveCreditsTbl').DataTable({
  "ajax": "php_actions/payroll_leave_credits.php"
});
function callModal(id,name,sl,vl,ml){
  $("input[name='id'").val(id);
  $("#fullName").html(name);
  $("input[name='sl'").val(sl);
  $("input[name='vl'").val(vl);
  $("input[name='ml'").val(ml);
}

$("#editLeaveCredits").submit(function(e){
  e.preventDefault();
  console.log('Form Submitted');
  var id = $("input[name='id'").val();
  var sl = $("input[name='sl'");
  var vl = $("input[name='vl'");
  var ml = $("input[name='ml'");
  var checkData = true;
  if (sl.val() == '') {
    checkData = false;
    sl.addClass('is-invalid');
  } else {
    sl.removeClass('is-invalid');
  }
  if (vl.val() == '') {
    checkData = false;
    vl.addClass('is-invalid');
  } else {
    vl.removeClass('is-invalid');
  }
  if (ml.val() == '') {
    checkData = false;
    ml.addClass('is-invalid');
  } else {
    ml.removeClass('is-invalid');
  }
  if (checkData && id > 0) {
    $.ajax({
      url: 'php_actions/modify_leave_credits.php',
      type: 'post',
      dataType: 'json',
      data: $(this).serialize(),
      success: function(result){
        $("#editLeaveCredits").trigger('reset');
        $("#close_modal").click();
        payrollLeaveCreditsDetailsTbl.ajax.reload();
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        if (result.status) {
          Toast.fire({
            icon: 'success',
            title: result.message
          });
        } else {
          Toast.fire({
            icon: 'error',
            title: result.message
          });
        }        
      }
    });
  }

});