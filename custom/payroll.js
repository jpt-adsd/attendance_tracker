var payrollDetailsTbl = $('#payrollTbl').DataTable({
  "ajax": "php_actions/payroll.php",
  dom: 'Bfrtip',
  buttons: [ 
      {
        text: 'Add Payroll',
        action: function ( e, dt, node, config ) {
          $("#modalTitle").html('Add Payroll');
          $("#reset").click()
          $('input[name="rate"]').attr('readonly', false);
          $('input[name="hoursWorked"]').attr('readonly', false);
          $('input[name^="amount_"]').attr('readonly', false);
          $('#modal-add-payroll').modal('show');
        }
      }
  ]
});
$('select[name="userId"]').change(function(e) {
  var inputUserId = $('select[name="userId"]').val();
  var startDate = $('input[name="startDate"]').val();
  var endDate = $('input[name="endDate"]').val();
  if (inputUserId) {
    $.ajax({
      url: 'php_actions/get_payroll_details.php',
      data: {id:inputUserId},
      type: 'post',
      dataType: 'json',
      success: function (data){
        $('input[name="rate"]').val(data.rate);
      }
    });
    if (startDate && endDate) {
      $.ajax({
        url: 'php_actions/get_hours.php',
        data: {id:inputUserId,startDate:startDate,endDate:endDate},
        type: 'post',
        dataType: 'json',
        success: function (data){
          if (data.status == 'true') {
              $('input[name="hoursWorked"]').val(parseFloat(data.hours.toFixed(2)));
              $("#submitAddPayroll").prop('disabled', false);
          } else {
            $("#submitAddPayroll").prop('disabled', true);
            var Toast = Swal.mixin({
              toast: true,
              positionClass: 'toast-top-center',
              showConfirmButton: false,
              timer: 3000
            });
            Toast.fire({
              icon: 'error',
              title: data.message
            });
          }          
        }
      });
    }
  }
});
$('input[name="startDate"]').change(function(e) {
  var inputUserId = $('select[name="userId"]').val();
  var startDate = $('input[name="startDate"]').val();
  var endDate = $('input[name="endDate"]').val();
  if (startDate && endDate && inputUserId) {
    $.ajax({
      url: 'php_actions/get_hours.php',
      data: {id:inputUserId,startDate:startDate,endDate:endDate},
      type: 'post',
      dataType: 'json',
      success: function (data){
        if (data.status == 'true') {
            $('input[name="hoursWorked"]').val(parseFloat(data.hours.toFixed(2)));
            $("#submitAddPayroll").prop('disabled', false);
        } else {
          $("#submitAddPayroll").prop('disabled', true);
          var Toast = Swal.mixin({
            toast: true,
            positionClass: 'toast-top-center',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            icon: 'error',
            title: data.message
          });
        }
      }
    });    
  }
});
$('input[name="endDate"]').change(function(e) {
  var inputUserId = $('select[name="userId"]').val();
  var startDate = $('input[name="startDate"]').val();
  var endDate = $('input[name="endDate"]').val();
  if (startDate && endDate && inputUserId) {
    $.ajax({
      url: 'php_actions/get_hours.php',
      data: {id:inputUserId,startDate:startDate,endDate:endDate},
      type: 'post',
      dataType: 'json',
      success: function (data){
        if (data.status == 'true') {
            $('input[name="hoursWorked"]').val(parseFloat(data.hours.toFixed(2)));
            $("#submitAddPayroll").prop('disabled', false);
        } else {
          $("#submitAddPayroll").prop('disabled', true);
          var Toast = Swal.mixin({
            toast: true,
            positionClass: 'toast-top-center',
            showConfirmButton: false,
            timer: 3000
          });
          Toast.fire({
            icon: 'error',
            title: data.message
          });
        }
      }
    });    
  }
});

$("#payrollAdd").submit(function(e){
  e.preventDefault();
  console.log('Form submitted');

  var totalSetup = $("input[name='total_setup']").val();
  var checkData = true;
  var id = $("select[name='id']");
  if (id.val() == '') {
    id.addClass('is-invalid');
    checkData = false;
  }
  var rate = $("input[name='rate']");
  if (rate.val() == '') {
    rate.addClass('is-invalid');
    checkData = false;
  }
  var hoursWorked = $("input[name='hoursWorked']");
  if (hoursWorked.val() == '') {
    hoursWorked.addClass('is-invalid');
    checkData = false;
  }
  var startDate = $("input[name='startDate']");
  if (startDate.val() == '') {
    startDate.addClass('is-invalid');
    checkData = false;
  }
  var endDate = $("input[name='endDate']");
  if (endDate.val() == '') {
    endDate.addClass('is-invalid');
    checkData = false;
  }
  if (startDate.val() > endDate.val()) {
    startDate.addClass('is-invalid');
    endDate.addClass('is-invalid');
    checkData = false;
  }
  
  if (checkData) {
    var saveCutoff = confirm('Do you want to save start and end date?');
    $.ajax({
      type: 'POST',
      url: 'php_actions/modify_payroll_details.php',
      dataType: 'json',
      data: $(this).serialize() + "&saveCutoff=" + saveCutoff,
      success: function(result) {
        $("#payrollAdd").trigger('reset');
        $("#modal_add_close").click();
        payrollDetailsTbl.ajax.reload();
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        if (result.status) {
          Toast.fire({
            icon: 'success',
            title: result.message
          });
        } else {
          Toast.fire({
            icon: 'error',
            title: result.message
          });
        }
      },
      error : function(error) {
          alert("here");
      }
    });
  }

});

function addField(type){
  var append;
  var count;
  if (type == 'add') {
    count = parseInt($('input[name="additionalAddCounter"]').val());
  } else {
    count = parseInt($('input[name="additionalCounter"]').val());
  }
  count += 1;
  append =  '<div class="form-group mb-3 row">';
  append += '  <div class="col-md-5">';
  append += '    <input type="text" class="form-control" placeholder="Name" name="name_add'+count+'">';
  append += '  </div>';
  append += '  <div class="col-md-7">';
  append += '    <input type="number" class="form-control" placeholder="Amount" name="rate_add'+count+'">';
  append += '  </div>';
  append += '</div>';
  if (type == 'add') {
    $("#additionalsPayroll").append(append);
    $('input[name="additionalAddCounter"]').val(count);
  } else {
    $("#additionals").append(append);
    $('input[name="additionalCounter"]').val(count);
  }
}

function modalPayroll(id,view = 0){
  var append;
  $.ajax({
    url: 'php_actions/get_payroll_details.php',
    method: 'post',
    dataType: 'json',
    data: {id:id},
    success: function(data){
      $("#modalTitlePayroll").html('Edit Payroll Details');
      $('input[name="id"]').val(data.id);
      $('#fullName').html(data.name);
      $("#additionals").html('');
      $("input[name='grossPay']").val('');
      $("input[name='netPay']").val('');
      $("#payrollDetails input").prop("readonly", false);
      $("#submit").show();
      $("#divPay").hide();
      if (data.cutoff) {
        append =  '<div class="form-group mb-3 row">';
        append += '  <div class="col-md-3"> <label for="cutoffSelection">Cut off: </label>';
        append += '  </div>';
        append += '  <div class="col-md-7">';
        append += '    <select class="form-control" id="cutoffSelection" name="cutoffSelection">';
        for (var x = 0; x < data.cutoff_id.length; x++) {
          append += '    <option value="'+data.cutoff_id[x]+'">'+data.cutoff_dates[x]+'</option>';
        }
        append += '    </select>';
        append += '  </div>';
        append += '  <div class="col-md-2">';
        append += '     <button type="button" class="btn btn-info" onclick="showPayrollDetail('+view+')">Show</button>';
        append += '  </div>';
        append += '</div>';
        append += '<div id="payrollDetailsResult">';
        append += '</div>';
        $("#additionals").append(append);
      } else {
        $("#additionals").append('<span>No payroll data.</span>');
      }
      if (view == 1) {
        $("#modalTitlePayroll").html('View Payroll Details');
        $("#payrollDetails input").prop("readonly", true);
        $("#divPay").show();
        $("#submit").hide();  
      }
    },
    error:  function (){
      alert('eng');
    }
  });
}

function showPayrollDetail(view){
  var payrollId = $('select[name="cutoffSelection"]').val();
  var append;
  $.ajax({
    url: 'php_actions/get_payroll_details_more.php',
    type: 'post',
    dataType: 'json',
    data: {payrollId:payrollId},
    success: function(data){
      $("#payrollDetailsResult").empty();
      //$("input[name='grossPay']").val(data.grossPay.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
      $("input[name='grossPay']").val(parseFloat(data.grossPay).toFixed(2));
      //$("input[name='netPay']").val(data.netPay.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);
      $("input[name='netPay']").val(parseFloat(data.netPay).toFixed(2));
      append =  '<div class="form-group mb-3 row">';
      append += '  <div class="col-md-3">';
      append += '    <label for="cutoffSelection">Hours Worked: </label>';
      append += '  </div>';
      append += '  <div class="col-md-3">';
      append += '    <input type="number" class="form-control" placeholder="Number of hours"';
       append += ' name="mHoursWorked" value="'+data.hoursWorked+'" step=".01">';
      append += '  </div>';
      append += '  <div class="col-md-3">';
      append += '    <label for="cutoffSelection">Rate: </label>';
      append += '  </div>';
      append += '  <div class="col-md-3">';
      append += '    <input type="number" class="form-control" placeholder="Rate"';
       append += ' name="mRate" value="'+data.rate+'" style="text-align:right;" step=".01">';
      append += '  </div>';
      append += '</div>';

      if (data.addOns) {
        for (var x = 0; x < data.name.length; x+=2) {
          append += '<div class="form-group mb-3 row">';
          append += '  <div class="col-md-3">';
          append += '    <label for="amount_'+data.setup_id[x]+'">'+data.name[x]+':</label>';
          append += '  </div>';
          append += '  <div class="col-md-3">';
          append += '    <input type="number" name="amount_'+data.setup_id[x]+'" step=".01" placeholder="Amount"';
          append += '      style="text-align: right;" class="form-control" value="'+data.amount[x]+'">';
          append += '  </div>';
            if (x+1 < data.name.length) {
            append += '<div class="col-md-3">';
            append += '  <label for="amount_'+data.setup_id[x+1]+'">'+data.name[x+1]+':</label>';
            append += '</div>';
            append += '<div class="col-md-3">';
            append += '  <input type="number" name="amount_'+data.setup_id[x+1]+'" step=".01" placeholder="Amount"';
            append += '    style="text-align: right;" class="form-control" value="'+data.amount[x+1]+'">';
            append += '</div>';
            append += '</div>';
            }
        }
      }
      $("#payrollDetailsResult").append(append);
      if (view == 1) {
        $('input[name="mRate"]').attr('readonly', true);
        $('input[name="mHoursWorked"]').attr('readonly', true);
        $('input[name^="amount_"').attr('readonly', true);
      }

    },
    error: function(){
      alert('showPayrollDetail()');
    }
  });
}

$("#payrollDetails").submit(function(e){
  e.preventDefault();
  console.log('Form submitted');

  var id = $("input[name='id']").val();
  var checkData = true;

  var rate = $("input[name='mRate']");
  if (rate.val() == '') {
    rate.addClass('is-invalid');
    checkData = false;
  } else {
    rate.removeClass('is-invalid');
  }
  var hoursWorked = $("input[name='mHoursWorked']");
  if (hoursWorked.val() == '') {
    hoursWorked.addClass('is-invalid');
    checkData = false;
  } else {
    hoursWorked.removeClass('is-invalid');
  }

  if (checkData) {
    $.ajax({
      type: 'POST',
      url: 'php_actions/modify_payroll_details.php',
      dataType: "json",
      data: $(this).serialize(),
      success: function(result) {
        payrollDetailsTbl.ajax.reload();
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        if (result.status) {
          Toast.fire({
            icon: 'success',
            title: result.message
          });
        } else {
          Toast.fire({
            icon: 'error',
            title: result.message
          });
        }
      },
      error : function(error) {
          alert("here");
      }
    });
  }
});

function modalPayrollDelete(id){
    $.ajax({
      url: 'php_actions/get_payroll_details.php',
      method: 'post',
      data: {id:id},
      dataType: 'json',
      success: function(data){
        $('input[name="dId"]').val(data.id);
        $("#deleteFullname").html(data.name);
        $('input[name="delete"]').val('true');
      }
    });
}

$("#deletePayrollDetails").submit(function(e){
  e.preventDefault();
  console.log('Form submitted');
  $.ajax({
    type: 'POST',
    url: 'php_actions/modify_payroll_details.php',
    dataType: "json",
    data: $(this).serialize(),
    success: function(result) {
      $(this).trigger("reset");
      $("#close_modal_delete").click();
      payrollDetailsTbl.ajax.reload();
      var Toast = Swal.mixin({
        toast: true,
        positionClass: 'toast-top-center',
        showConfirmButton: false,
        timer: 3000
      });
      if (result.status) {
        Toast.fire({
          icon: 'success',
          title: result.message
        });
      } else {
        Toast.fire({
          icon: 'error',
          title: result.message
        });
      }
    },
    error : function(error) {
        alert("here");
    }
  });
});