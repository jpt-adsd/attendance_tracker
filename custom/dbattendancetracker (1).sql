-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2021 at 11:36 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbattendancetracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladditionalsonpayrolls`
--

CREATE TABLE `tbladditionalsonpayrolls` (
  `id` int(11) NOT NULL,
  `payrollId` int(11) NOT NULL,
  `payrollSetupId` int(11) NOT NULL,
  `amount` decimal(10,2) DEFAULT 0.00,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tbladditionalsonpayrolls`
--

INSERT INTO `tbladditionalsonpayrolls` (`id`, `payrollId`, `payrollSetupId`, `amount`, `status`) VALUES
(78, 39, 11, '500.00', 1),
(79, 39, 12, '200.00', 1),
(80, 39, 13, '175.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblfileleaves`
--

CREATE TABLE `tblfileleaves` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `leaveTypeId` int(11) NOT NULL,
  `numberOfLeaves` int(11) NOT NULL,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `hrUserId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tblleavecredits`
--

CREATE TABLE `tblleavecredits` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `leaveId` int(11) NOT NULL,
  `credit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblleavecredits`
--

INSERT INTO `tblleavecredits` (`id`, `userId`, `leaveId`, `credit`) VALUES
(8, 40, 1, 0),
(9, 40, 2, 0),
(10, 40, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblleavestype`
--

CREATE TABLE `tblleavestype` (
  `id` int(11) NOT NULL,
  `leavesType` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblleavestype`
--

INSERT INTO `tblleavestype` (`id`, `leavesType`) VALUES
(3, 'MATERNITY LEAVE'),
(1, 'SICK LEAVE'),
(2, 'VACATION LEAVE');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogs`
--

CREATE TABLE `tbllogs` (
  `id` int(11) NOT NULL,
  `personnelUserId` int(11) NOT NULL,
  `dateTimeIn` datetime NOT NULL,
  `dateTimeOut` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tblpayrollcompensation`
--

CREATE TABLE `tblpayrollcompensation` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `isDeduction` int(11) NOT NULL,
  `status` int(11) DEFAULT 1,
  `dateCreated` datetime DEFAULT current_timestamp(),
  `dateUpdated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblpayrollcompensation`
--

INSERT INTO `tblpayrollcompensation` (`id`, `name`, `isDeduction`, `status`, `dateCreated`, `dateUpdated`) VALUES
(11, 'SSS', 1, 1, '2021-05-27 17:07:19', NULL),
(12, 'PAGIBIG', 1, 1, '2021-05-27 17:07:30', NULL),
(13, 'PHILHEALTH', 1, 1, '2021-05-27 17:07:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblpayrolls`
--

CREATE TABLE `tblpayrolls` (
  `id` int(11) NOT NULL,
  `payrollUserId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` datetime DEFAULT NULL,
  `hrsWorked` decimal(10,2) NOT NULL,
  `rate` decimal(10,2) NOT NULL,
  `cutoffStartDate` date NOT NULL,
  `cutoffEndDate` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblpayrolls`
--

INSERT INTO `tblpayrolls` (`id`, `payrollUserId`, `dateCreated`, `dateUpdated`, `hrsWorked`, `rate`, `cutoffStartDate`, `cutoffEndDate`, `status`) VALUES
(38, 40, '2021-05-27 17:02:34', NULL, '80.00', '500.00', '2021-06-01', '2021-06-15', 1),
(39, 40, '2021-05-27 17:08:17', NULL, '90.00', '500.00', '2021-06-01', '2021-06-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblpositions`
--

CREATE TABLE `tblpositions` (
  `id` int(11) NOT NULL,
  `position` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblpositions`
--

INSERT INTO `tblpositions` (`id`, `position`) VALUES
(1, 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `tblroles`
--

CREATE TABLE `tblroles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblroles`
--

INSERT INTO `tblroles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'payroll'),
(3, 'hr'),
(4, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `id` int(11) NOT NULL,
  `employeeId` varchar(255) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `contactNumber` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT current_timestamp(),
  `dateUpdated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `roleId` int(11) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`id`, `employeeId`, `userName`, `firstName`, `lastName`, `contactNumber`, `dateCreated`, `dateUpdated`, `status`, `roleId`, `password`) VALUES
(40, '1001', 'ariel', 'ARIEL', 'STO DOMINGO', '09123456789', '2021-05-27 17:01:11', NULL, 1, 1, '757e9a59dd06e110166e044d733c2bd7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladditionalsonpayrolls`
--
ALTER TABLE `tbladditionalsonpayrolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblfileleaves`
--
ALTER TABLE `tblfileleaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblleavecredits`
--
ALTER TABLE `tblleavecredits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblleavestype`
--
ALTER TABLE `tblleavestype`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `leavesType` (`leavesType`);

--
-- Indexes for table `tbllogs`
--
ALTER TABLE `tbllogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpayrollcompensation`
--
ALTER TABLE `tblpayrollcompensation`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tblpayrolls`
--
ALTER TABLE `tblpayrolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpositions`
--
ALTER TABLE `tblpositions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `position` (`position`);

--
-- Indexes for table `tblroles`
--
ALTER TABLE `tblroles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_id` (`employeeId`),
  ADD UNIQUE KEY `user_name` (`userName`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladditionalsonpayrolls`
--
ALTER TABLE `tbladditionalsonpayrolls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `tblfileleaves`
--
ALTER TABLE `tblfileleaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblleavecredits`
--
ALTER TABLE `tblleavecredits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblleavestype`
--
ALTER TABLE `tblleavestype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbllogs`
--
ALTER TABLE `tbllogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpayrollcompensation`
--
ALTER TABLE `tblpayrollcompensation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tblpayrolls`
--
ALTER TABLE `tblpayrolls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tblpositions`
--
ALTER TABLE `tblpositions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblroles`
--
ALTER TABLE `tblroles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
