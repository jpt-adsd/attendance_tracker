var usersTbl = $('#users').DataTable({
  "ajax": "php_actions/users.php",
  dom: 'Bfrtip',
  buttons: [ 
      {
        text: 'Add User',
          action: function ( e, dt, node, config ) {
            $("#modalTitle").html('Add New User');
            $('input[name="id"]').prop('readonly', false).removeClass('is-invalid');
            $('input[name="firstName"]').prop('readonly', false).removeClass('is-invalid');
            $('input[name="lastName"]').prop('readonly', false).removeClass('is-invalid');
            $('input[name="contactNumber"]').prop('readonly', false).removeClass('is-invalid');
            $('input[name="userName"]').prop('readonly', false).removeClass('is-invalid');
            $('input[name="employeeId"]').prop('readonly', false).removeClass('is-invalid');
            $("#role").prop('disabled', false).removeClass('is-invalid');
            $("#reset").click()
            $('#modal-default').modal('show');
          }
      }
  ]
});

function callUserModal(id,view){
  $.ajax({
    url: 'php_actions/get_user.php',
    method: 'post',
    data: {id:id},
    dataType: 'json',
    success: function(data){
      $("#modalTitle").html('Edit User');
      $('input[name="id"]').val(data.id).prop('readonly', false).removeClass('is-invalid');
      $('input[name="firstName"]').val(data.firstName).prop('readonly', false).removeClass('is-invalid');
      $('input[name="lastName"]').val(data.lastName).prop('readonly', false).removeClass('is-invalid');
      $('input[name="contactNumber"]').val(data.contactNumber).prop('readonly', false).removeClass('is-invalid');
      $('input[name="userName"]').val(data.userName).prop('readonly', false).removeClass('is-invalid');
      $('input[name="employeeId"]').val(data.employeeId).prop('readonly', false).removeClass('is-invalid');
      $("#role").val(data.role).prop('disabled', false).removeClass('is-invalid');
      if (view == 1) {
        $("#modalTitle").html('View User');
        $('input[name="id"]').prop('readonly', true).removeClass('is-invalid');
        $('input[name="firstName"]').prop('readonly', true).removeClass('is-invalid');
        $('input[name="lastName"]').prop('readonly', true).removeClass('is-invalid');
        $('input[name="contactNumber"]').prop('readonly', true).removeClass('is-invalid');
        $('input[name="userName"]').val(data.userName).prop('readonly', true).removeClass('is-invalid');
        $('input[name="employeeId"]').val(data.employeeId).prop('readonly', true).removeClass('is-invalid');
        $("#role").prop('disabled', true).removeClass('is-invalid');
        $("#submit").hide();
        $("#reset").hide();
      }
    }
  });
}

function callUserDeleteModal(id){
  $.ajax({
    url: 'php_actions/get_user.php',
    method: 'post',
    data: {id:id},
    dataType: 'json',
    success: function(data){
      $('input[name="id"]').val(data.id);
      $("#deleteFullname").html(data.firstName + ' ' + data.lastName);
      $('input[name="delete"]').val('true');
    }
  });
}

function updateStatus(id,status){
  var update = 'true';
  $.ajax({
    type: 'POST',
    url: 'php_actions/modify_user.php',
    dataType: "json",
    data: {id:id, update:update, status:status},
    success: function(result) {
      usersTbl.ajax.reload();
      var Toast = Swal.mixin({
        toast: true,
        positionClass: 'toast-top-center',
        showConfirmButton: false,
        timer: 3000
      });
      if (result.status) {
        Toast.fire({
          icon: 'success',
          title: result.message
        });
      } else {
        Toast.fire({
          icon: 'error',
          title: result.message
        });
      }
    },
    error : function(error) {
        alert("here");
    }
  });
}

$(document).ready(function() {
  $("#deleteUser").submit(function(e){
    e.preventDefault();
    console.log('Form submitted');
    $.ajax({
      type: 'POST',
      url: 'php_actions/modify_user.php',
      dataType: "json",
      data: $(this).serialize(),
      success: function(result) {
        $(this).trigger("reset");
        $("#close_modal_delete").click();
        usersTbl.ajax.reload();
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        if (result.status) {
          Toast.fire({
            icon: 'success',
            title: result.message
          });
        } else {
          Toast.fire({
            icon: 'error',
            title: result.message
          });
        }
      },
      error : function(error) {
          alert("here");
      }
    });
  });
  $('#addUser').validate({
    rules: {
      userName: {
        required: true,
      },
      employeeId: {
        required: true,
      },
      firstName: {
        required: true,
      },
      lastName: {
        required: true,
      },
      contactNumber: {
        required: true,
      },
      position: {
        required: true,
      },
      role: {
        required: true,
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },
    submitHandler: function(form, e) {
      e.preventDefault();
      console.log('Form submitted');
      $.ajax({
        type: 'POST',
        url: 'php_actions/modify_user.php',
        dataType: "json",
        data: $('#addUser').serialize(),
        success: function(result) {
          $("#addUser").trigger("reset");
          $("#close_modal").click();
          usersTbl.ajax.reload();
          var Toast = Swal.mixin({
            toast: true,
            positionClass: 'top-center',
            showConfirmButton: false,
            timer: 3000
          });
          if (result.status) {
            var timing = 3000;
            var showMessage = '';
            if (result.action == 'add') {
              timing = 300000;
              showMessage = '<h2>The temporary password is <b>' + result.temp_password + '</b>.</h2>';
            }
            Toast.fire({
              icon: 'success',
              title: result.message,
              timer: timing,
              html: showMessage,
              showCloseButton: true
            });
          } else {
            Toast.fire({
              icon: 'error',
              title: result.message
            });
          }
        },
        error : function(error) {
          alert("here");
        }
      });
      return false;
    }
  });
});