-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 19, 2021 at 10:10 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbattendancetracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladditionalsonpayrolls`
--

DROP TABLE IF EXISTS `tbladditionalsonpayrolls`;
CREATE TABLE IF NOT EXISTS `tbladditionalsonpayrolls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `field` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tbladditionalsonpayrolls`
--

INSERT INTO `tbladditionalsonpayrolls` (`id`, `userId`, `field`, `name`, `amount`) VALUES
(2, 33, 'add1', 'RATE', '500.00'),
(5, 34, 'add1', 'rate', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `tblfileleaves`
--

DROP TABLE IF EXISTS `tblfileleaves`;
CREATE TABLE IF NOT EXISTS `tblfileleaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `leaveTypeId` int(11) NOT NULL,
  `numberOfLeaves` int(11) NOT NULL,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `hrUserId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tblleavecredits`
--

DROP TABLE IF EXISTS `tblleavecredits`;
CREATE TABLE IF NOT EXISTS `tblleavecredits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `leaveId` int(11) NOT NULL,
  `credit` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tblleavestype`
--

DROP TABLE IF EXISTS `tblleavestype`;
CREATE TABLE IF NOT EXISTS `tblleavestype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `leavesType` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `leavesType` (`leavesType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tbllogs`
--

DROP TABLE IF EXISTS `tbllogs`;
CREATE TABLE IF NOT EXISTS `tbllogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personnelUserId` int(11) NOT NULL,
  `dateTimeIn` datetime NOT NULL,
  `dateTimeOut` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tblpayrolls`
--

DROP TABLE IF EXISTS `tblpayrolls`;
CREATE TABLE IF NOT EXISTS `tblpayrolls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payrollUserId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime DEFAULT NULL,
  `hrsWorked` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `tblpositions`
--

DROP TABLE IF EXISTS `tblpositions`;
CREATE TABLE IF NOT EXISTS `tblpositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `position` (`position`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblpositions`
--

INSERT INTO `tblpositions` (`id`, `position`) VALUES
(1, 'ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `tblroles`
--

DROP TABLE IF EXISTS `tblroles`;
CREATE TABLE IF NOT EXISTS `tblroles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblroles`
--

INSERT INTO `tblroles` (`id`, `role`) VALUES
(1, 'admin'),
(2, 'payroll'),
(3, 'hr'),
(4, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

DROP TABLE IF EXISTS `tblusers`;
CREATE TABLE IF NOT EXISTS `tblusers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `contactNumber` varchar(255) NOT NULL,
  `positionId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `roleId` int(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf16;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`id`, `firstName`, `lastName`, `contactNumber`, `positionId`, `dateCreated`, `dateUpdated`, `status`, `roleId`, `password`) VALUES
(33, '1', '23', '3462', 1, '2021-05-18 18:07:01', NULL, 1, 1, 'df3cd7cc93961b876be7e48744169ca6'),
(34, '2', '2', '2', 1, '2021-05-18 18:32:26', NULL, 2, 2, 'a74b29ba393f9d2e38a6c97a6344172d');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
