var payrollDetailsTbl = $('#payrollTbl').DataTable({
  "ajax": "php_actions/payroll.php"
});

function addField(){
  var append;
  var count = parseInt($('input[name="additionalCounter"]').val());
  count += 1;
  append =  '<div class="form-group mb-3 row">';
  append += '  <div class="col-md-5">';
  append += '    <input type="text" class="form-control" placeholder="Name" name="name_add'+count+'">';
  append += '  </div>';
  append += '  <div class="col-md-7">';
  append += '    <input type="number" class="form-control" placeholder="Amount" name="rate_add'+count+'">';
  append += '  </div>';
  append += '</div>';
  $("#additionals").append(append);
  $('input[name="additionalCounter"]').val(count);
}

function modalPayroll(id,view){
  var append;
  $.ajax({
    url: 'php_actions/get_payroll_details.php',
    method: 'post',
    dataType: 'json',
    data: {id:id},
    success: function(data){
      $("#modalTitle").html('Edit Payroll Details');
      $('input[name="id"]').val(data.id);
      $('#fullName').html(data.name);
      $('input[name="rate"]').val(data.rate);
      $("#additionals").html('');
      $("#payrollDetails input").prop("readonly", false);
      $("#submit").show();
      $("#btn_additionals").show();
      $('input[name="additionalCounter"]').val('0');
      var countAdditional = parseInt($('input[name="additionalCounter"]').val());
      for (var i = 0; i < data.field.length; i++) {
        countAdditional += 1;
        append =  '<div class="form-group mb-3 row">';
        append += '  <div class="col-md-5">';
        if (i > 0) {
          append += '    <input type="text" class="form-control" placeholder="Name" name="name_'
          +data.field[i]+'" value="'+data.addName[i].toUpperCase()+'">';  
        } else {
          append += '    <input type="text" class="form-control" placeholder="Name" name="name_'
          +data.field[i]+'" value="'+data.addName[i].toUpperCase()+'" readonly>';  
        }
        
        append += '  </div>';
        append += '  <div class="col-md-7">';
        append += '    <input type="number" class="form-control" placeholder="Amount" name="rate_'
          +data.field[i]+'" value="'+data.amount[i]+'">';
        append += '  </div>';
        append += '</div>';
        $("#additionals").append(append);
      }
      $('input[name="additionalCounter"]').val(countAdditional);
      $('input[name="oldCounter"]').val(countAdditional);
      if (view == 1) {
        $("#modalTitle").html('View Payroll Details');
        $("#payrollDetails input").prop("readonly", true);
        $("#submit").hide();
        $("#btn_additionals").hide();
      }
    },
    error:  function (){
      alert('eng');
    }
  });
}

$("#payrollDetails").submit(function(e){
  e.preventDefault();
  console.log('Form submitted');

  var id = $("input[name='id']").val();
  var old = parseInt($("input[name='oldCounter']").val());
  var additionals = $("input[name='additionalCounter']").val();
  var checkData = true;

  var addRate = $("input[name='rate_add1']");
  if (addRate.val() == '') {
    addRate.addClass('is-invalid');
    checkData = false;
  }

  for (var i = 2; i <= additionals; i++) {
    var addName = $("input[name='name_add"+i+"']");
    var addRate = $("input[name='rate_add"+i+"']");
    if (addName.val() == '' || addRate.val() == '') {
      if (addName.val() == '') {
        addName.addClass('is-invalid');
        checkData = false;
      } else {
        addRate.addClass('is-invalid');
        checkData = false;
      }

      if (i > 2) {
        for (var x = 2; x < i; x++) {
          var addNameCheck = $("input[name='name_add"+x+"']");
          var addRateCheck = $("input[name='rate_add"+x+"']");
            if (addNameCheck.val() == '') {
              addNameCheck.addClass('is-invalid');
              checkData = false;
            } else {
              addNameCheck.removeClass('is-invalid');
            } 
            if (addRateCheck.val() == '') {
              addRateCheck.addClass('is-invalid');
              checkData = false;
            } else {
              addRateCheck.removeClass('is-invalid');
            }
        }
      }
    } else {
      addName.removeClass('is-invalid');
      addRate.removeClass('is-invalid');
    }
  }

  if (checkData) {
    $.ajax({
      type: 'POST',
      url: 'php_actions/modify_payroll_details.php',
      dataType: "json",
      data: $(this).serialize(),
      success: function(result) {
        $(this).trigger("reset");
        $("#close_modal").click();
        payrollDetailsTbl.ajax.reload();
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        if (result.status) {
          Toast.fire({
            icon: 'success',
            title: result.message
          });
        } else {
          Toast.fire({
            icon: 'error',
            title: result.message
          });
        }
      },
      error : function(error) {
          alert("here");
      }
    });
  }
  
});

function modalPayrollDelete(id){
    $.ajax({
      url: 'php_actions/get_payroll_details.php',
      method: 'post',
      data: {id:id},
      dataType: 'json',
      success: function(data){
        $('input[name="id"]').val(data.id);
        $("#deleteFullname").html(data.name);
        $('input[name="delete"]').val('true');
      }
    });
}

$("#deletePayrollDetails").submit(function(e){
  e.preventDefault();
  console.log('Form submitted');
  $.ajax({
    type: 'POST',
    url: 'php_actions/modify_payroll_details.php',
    dataType: "json",
    data: $(this).serialize(),
    success: function(result) {
      $(this).trigger("reset");
      $("#close_modal_delete").click();
      payrollDetailsTbl.ajax.reload();
      var Toast = Swal.mixin({
        toast: true,
        positionClass: 'toast-top-center',
        showConfirmButton: false,
        timer: 3000
      });
      if (result.status) {
        Toast.fire({
          icon: 'success',
          title: result.message
        });
      } else {
        Toast.fire({
          icon: 'error',
          title: result.message
        });
      }
    },
    error : function(error) {
        alert("here");
    }
  });
});