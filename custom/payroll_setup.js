var payrollDetailsTbl = $('#payrollSetupTbl').DataTable({
  "ajax": "php_actions/payroll_setup.php",
  dom: 'Bfrtip',
  buttons: [ 
      {
        text: 'Add New Compensation',
          action: function ( e, dt, node, config ) {
            $("#modalTitle").html('Add New Compensation');
            $('input[name="type"').val('add');
            $('#modal-default').modal('show');
          }
      }
  ]
});
function getCompensationData(id,deletion){
  $.ajax({
    type: 'POST',
    url: 'php_actions/get_compensation.php',
    dataType: "json",
    data: {id:id},
    success: function(result) {
      if (deletion == 1) {
        $("input[name='dId']").val(result.id);
        $('input[name="dStatus"').val('2');
        $("#deleteCompensationName").html(result.name);
      } else {
        $("input[name='id']").val(result.id);
        $('input[name="type"').val('edit');
        $("input[name='name']").val(result.name);
        $('input:radio[name="category"]'). filter('[value="'+result.isDeduction+'"]'). attr('checked', true);
        $('input:radio[name="status"]'). filter('[value="'+result.status+'"]'). attr('checked', true);
      }
    }
  });
}

$("#deleteCompensation").submit(function(e){
  e.preventDefault();
  console.log('Form submitted');
  var id = $("input[name='dId']").val();
  var status = $('input[name="dStatus"').val();
  if (id > 0 && status == 2) {
    updateStatus(id,status);
  }  
});

function updateStatus(id,status){
  $.ajax({
    type: 'POST',
    url: 'php_actions/modify_compensation.php',
    dataType: "json",
    data: {id:id, status:status},
    success: function(result) {
      if (status == 2) {
        $("#close_modal_delete").click();
      }
      payrollDetailsTbl.ajax.reload();
      var Toast = Swal.mixin({
        toast: true,
        positionClass: 'toast-top-center',
        showConfirmButton: false,
        timer: 3000
      });
      if (result.status) {
        Toast.fire({
          icon: 'success',
          title: result.message
        });
      } else {
        Toast.fire({
          icon: 'error',
          title: result.message
        });
      }
    },
    error : function(error) {
        alert("Something error.");
    }
  });

}

$("#compensation").on('submit', function(e){
  e.preventDefault();
  console.log('Form submitted');
  var name = $('input[name="name"');
  var category = $('input[name="category"');
  var status = $('input[name="status"');
  var type = $('input[name="type"');
  var checkData = true;
  if (name.val() == '') {
    checkData = false;
    name.addClass('is-invalid');
  }
  if (category.val() == '') {
    checkData = false;
    category.addClass('is-invalid');
  }
  if (status.val() == '') {
    checkData = false;
    status.addClass('is-invalid');
  }
  if (checkData && type) {
    $.ajax({
      type: 'POST',
      url: 'php_actions/modify_compensation.php',
      dataType: "json",
      data: $(this).serialize(),
      success: function(result) {
        $(this).trigger("reset");
        $('#modal-default').modal('hide');
        payrollDetailsTbl.ajax.reload();
        var Toast = Swal.mixin({
          toast: true,
          positionClass: 'toast-top-center',
          showConfirmButton: false,
          timer: 3000
        });
        if (result.status) {
          Toast.fire({
            icon: 'success',
            title: result.message
          });
        } else {
          Toast.fire({
            icon: 'error',
            title: result.message
          });
        }
      },
      error : function(error) {
          alert("Something error.");
      }
    });
  }
});