<?php include 'layout/header.php';?>

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Users Management</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= $_SESSION['home'] ?>">Home</a></li>
              <li class="breadcrumb-item active">Users Management</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card row">
              <div class="card-header">
                <h3 class="card-title">Users information</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body col-md-12">
                <table id="users" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                  	<th scope="col">Emp ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Contact #</th>
                    <th scope="col">Since</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                  </thead>
                  <tfoot>
                  <tr>
                  	<th>#</th>
                    <th>Name</th>
                    <th>Contact #</th>
                    <th>Since</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
	</section>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalTitle"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: auto;max-height: 400px;">
        <form id="addUser">
        	<input type="hidden" name="id">
          <div class="form-group mb-3">
            <label for="empId">Employee ID</label>
            <input type="text" class="form-control" placeholder="Employee ID" name="employeeId" autocomplete="off">
          </div>
          <div class="form-group mb-3">
            <label for="userName">User Name</label>
            <input type="text" class="form-control" placeholder="User name" name="userName" autocomplete="off">
          </div>
          <div class="form-group mb-3">
            <label for="firstName">First Name</label>
            <input type="text" class="form-control" placeholder="First name" name="firstName" autocomplete="off">
          </div>
          <div class="form-group mb-3">
            <label for="lastName">Last Name</label>
            <input type="text" class="form-control" placeholder="Last name" name="lastName" autocomplete="off">
          </div>
          <div class="form-group mb-3">
            <label for="contactNumber">Contact Number</label>
            <input type="text" class="form-control" placeholder="Contact #" name="contactNumber" autocomplete="off">
          </div>
          <div class="form-group mb-3">
            <label for="role">Role</label>
            <select class="form-control" name="role" id="role">
            	<option value="">~~~Select Role~~~</option>
            	<?php
            		$qry = "SELECT * FROM tblRoles ORDER BY role";
            		$result = $conn->query($qry);
            		while ($row = $result->fetch_array()) {
            			echo '<option value="'.$row['id'].'">'.strtoupper($row['role']).'</option>';
            		}
            	?>
            </select>
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
        <button type="reset" name="reset" id="reset" class="btn btn-warning">Reset</button>
        <button type="button" data-dismiss="modal" id="close_modal" class="btn btn-danger">Cancel</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="modal fade" id="modal-delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
      	<div class="alert alert-danger alert-dismissible">
          <h5><i class="icon fas fa-ban"></i> Delete confirmation!</h5>
          The action is irreversible! Please confirm to delete user.<br>
          <span id="deleteFullname"></span>
        </div>
        <form id="deleteUser">
        	<input type="hidden" name="id">
        	<input type="hidden" name="delete">
      </div>
      <div class="modal-footer">
        <button type="submit" name="submit" class="btn btn-danger">Confirm</button>
        <button type="button" data-dismiss="modal" id="close_modal_delete" class="btn btn-primary">Cancel</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php include 'layout/footer.php'; ?>
<script src="custom/users.js"></script>
<?php
  if ($_SESSION['role'] != 'admin') {
    echo '<script>usersTbl.buttons().nodes().css("display", "none");</script>';
  }
?>
